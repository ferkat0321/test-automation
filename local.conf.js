//  The url for the test to execute is set as a variable, if you need to run on
// Prod environment you need to set url=prod and if you need to run on IMP0
// you need to set baseUrl = imp0 on line 34


var prod = 'https://qpp.cms.gov/',
    imp0 = 'https://imp0.qpp.cms.gov/?ACA=PrW9Ccxk49',
    val0 = 'http://val0-webelb-677044720.us-east-1.elb.amazonaws.com',
    dev1 ='http://10.224.24.21/';

exports.config = {
    directConnect: false,


    capabilities: {
        'browserName': 'phantomjs',
        'phantomjs.binary.path': './node_modules/phantomjs/lib/phantom/bin/phantomjs',
        'shardTestFiles': true,
        'chromeOptions': {
            args: ['--window-size=1920,1080']

        },
        maxInstances: 1,
    },

    framework: 'jasmine',



     specs: ['./test_suite/e2e/specs/regression_test/*/*.js'],
     exclude: ['./test_suite/e2e/specs/regression_test/signin_page/*.js'],
     //specs:['./test_suite/e2e/specs/regression_test/page_verification/verify_page_header_footer_test.js'],
    //specs: ['./test_suite/e2e/specs/regression_test/explore_measures_page/verify_Explore_Measures_page_Specialty_Measure_Set_dropdown_test.js'],


    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000,
        showColors: true
    },

    baseUrl: imp0,

    onPrepare: function () {

        var MailListener = require("mail-listener2");

        // here goes your email connection configuration
        var mailListener = new MailListener({
            username: "tistaqpp@hotmail.com",
            password: "tista123$",
            host: "imap-mail.outlook.com",
            port: 993, // imap port
            tls: true,
            tlsOptions: {rejectUnauthorized: false},
            mailbox: "Inbox", // mailbox to monitor
            //searchFilter: ["Unread"], // the search filter being used after an IDLE notification has been retrieved
            markSeen: true, // all fetched email willbe marked as seen and not fetched next time
            fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
            mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
            attachments: true, // download attachments as they are encountered to the project directory
            attachmentOptions: {directory: "attachments/"} // specify a download directory for attachments
        });

        //mailListener.start();

       // mailListener.on("server:connected", function () {
            //console.log("Mail listener initialized");
        //});

        //global.mailListener = mailListener;

        var origFn = browser.driver.controlFlow().execute;

        jasmine.getEnv().beforeEach(function () {
            browser.ignoreSynchronization = true;

            browser.driver.controlFlow().execute = function () {
                var args = arguments;
                origFn.call(browser.driver.controlFlow(), function () {
                    return protractor.promise.delayed(20);
                });

                return origFn.apply(browser.driver.controlFlow(), args);
            };
        });


        var AllureReporter = require('jasmine-allure-reporter');

        jasmine.getEnv().addReporter(new AllureReporter({
            resultsDir: './Functional_Regression/allure_results'
        }));


        jasmine.getEnv().afterEach(function (done) {

            var myReporter = {
                specDone: function (result) {

                    allure.feature(result.fullName);

                    console.log("Result name: " + result.fullName);

                    if (result.status == "passed") {
                        allure.story("You're good now.");

                    }
                    else {
                        allure.story("Something gone wrong!");
                    }
                    return result;
                },

            }

            jasmine.getEnv().addReporter(myReporter);

            browser.takeScreenshot().then(function (png) {
                allure.createAttachment('Screenshot', function () {
                    return new Buffer(png, 'base64')
                }, 'image/png')();
                done();
            })

        });

    }


};
