//  The url for the test to execute is set as variable, if you need to run on
// Prod environment you need to set url=prod and if you need to run on IMP0
// you need to set url = imp0 on line 12

var prod = 'https://qpp.cms.gov/';
var imp0 = 'https://imp0.qpp.cms.gov/?ACA=PrW9Ccxk49';
var val0='http://val0-webelb-677044720.us-east-1.elb.amazonaws.com';

exports.config = {
    directConnect: false,

    baseUrl:imp0,


    sauceUser: 'QPPIT',
    sauceKey:'991ae9fa-e6a9-418c-ae63-79e9bd9eefb4',


    // Capabilities to be passed to the webdriver instance.
    // Right now we have Win 10, Win 8.1, OSX 10.10
    // and latest 2 versions for each OS
    multiCapabilities: [

        ////////////////------------Windows 10------------////////////////

        // {
        //     browserName: 'firefox',
        //     version: '50',
        //     platform: 'Windows 10',
        //     name: "firefox50-Windows 10",
        //     shardTestFiles: true,
        //     maxInstances: 1,
        //
        //     'build': 123
        // },

        {
            browserName: 'firefox',
            version: '46',
            platform: 'Windows 10',
            name: "firefox49-Windows 10",
            shardTestFiles: true,
            maxInstances: 25,

            'build': 123
        },

        // {
        //     browserName: 'chrome',
        //     version: '54',
        //     platform: 'Windows 10',
        //     name: "Chrome54-Windows 10",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'chrome',
        //     version: '53',
        //     platform: 'Windows 10',
        //     name: "Chrome53-Windows 10",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'Microsoft edge',
        //     version: '14.14393',
        //     platform: 'Windows 10',
        //     name: "Edge-Windows 10",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },

        // {
        //     browserName: 'Internet explorer',
        //     version: '11',
        //     platform: 'Windows 10',
        //     name: "IE-Windows 10",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },

        // ////////////////------------Windows 8.1------------////////////////
        //
        // {
        //     browserName: 'firefox',
        //     version: '50',
        //     platform: 'Windows 8.1',
        //     name: "firefox50-Windows 8.1",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'firefox',
        //     version: '49',
        //     platform: 'Windows 8.1',
        //     name: "firefox49-Windows 8.1",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'chrome',
        //     version: '54',
        //     platform: 'Windows 8.1',
        //     name: "Chrome54-Windows 8.1",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'chrome',
        //     version: '53',
        //     platform: 'Windows 10',
        //     name: "Chrome53-Windows 10",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'IE',
        //     version: '11',
        //     platform: 'Windows 8.1',
        //     name: "IE11-Windows 8.1",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // ////////////////------------OSX 10.11------------////////////////
        //
        // {
        //     browserName: 'firefox',
        //     version: '47',
        //     platform: 'OS X 10.11',
        //     name: "firefox-osx10.11",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'firefox',
        //     version: '49',
        //     platform: 'OS X 10.11',
        //     name: "firefox-osx10.11",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'chrome',
        //     version: '54',
        //     platform: 'OS X 10.11',
        //     name: "Chrome-OSX10.11",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'chrome',
        //     version: '53',
        //     platform: 'OS X 10.11',
        //     name: "Chrome-OSX10.11",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // },
        //
        // {
        //     browserName: 'safari',
        //     version: '10',
        //     platform: 'OS X 10.11',
        //     name: "Chrome-OSX10.11",
        //     shardTestFiles: true,
        //     maxInstances: 25,
        //
        //     'build': 123
        // }


    ],

    framework: 'jasmine',
    //specs: ['./test_suite/e2e/specs/regression_test/page_verification/verify_page_header_footer_test.js'],

    specs: ['./test_suite/e2e/specs/regression_test/*/*.js'],
    exclude: ['./test_suite/e2e/specs/regression_test/signin_page/*.js'],

    jasmineNodeOpts: {
        defaultTimeoutInterval: 300000,
        showColors: true
    },

    onPrepare: function () {

        // var MailListener = require("mail-listener2");
        //
        // // here goes your email connection configuration
        // var mailListener = new MailListener({
        //     username: "tistaqpp@hotmail.com",
        //     password: "tista123$",
        //     host: "imap-mail.outlook.com",
        //     port: 993, // imap port
        //     tls: true,
        //     tlsOptions: { rejectUnauthorized: false },
        //     mailbox: "Inbox", // mailbox to monitor
        //     //searchFilter: ["Unread"], // the search filter being used after an IDLE notification has been retrieved
        //     markSeen: true, // all fetched email willbe marked as seen and not fetched next time
        //     //fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
        //     mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
        //     attachments: true, // download attachments as they are encountered to the project directory
        //     attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments
        // });
        //
        // mailListener.start();
        //
        // mailListener.on("server:connected", function(){
        //     //console.log("Mail listener initialized");
        // });
        //
        // global.mailListener = mailListener;

        var caps = browser.getCapabilities();

        var origFn = browser.driver.controlFlow().execute;

        jasmine.getEnv().beforeEach(function () {
            browser.ignoreSynchronization = true;
            browser.driver.controlFlow().execute = function() {
                var args = arguments;

                origFn.call(browser.driver.controlFlow(), function() {
                    return protractor.promise.delayed(500);
                });

                return origFn.apply(browser.driver.controlFlow(), args);
            };
        });

        var AllureReporter = require('jasmine-allure-reporter');

        jasmine.getEnv().addReporter(new AllureReporter({
            resultsDir: './Functional_Regression/allure_results'
        }));
        jasmine.getEnv().afterEach(function (done) {


            var myReporter = {
                specDone: function (result) {
                    // if (url == imp0) {
                    //     allure.addEnvironment('This test is on IMPL environment.', imp0);
                    // }
                    //
                    // else if (url == prod) {
                    //     allure.addEnvironment('This test is on prod environment.', prod);
                    // }

                    allure.feature(result.fullName);

                    console.log("Result name: " + result.fullName);

                    if (result.status == "passed") {
                        allure.story("You're good now.");

                    }
                    else {
                        allure.story("Something gone wrong!");
                    }
                    return result;
                },

            }

            jasmine.getEnv().addReporter(myReporter);
            console.log("Test passed!");
            browser.takeScreenshot().then(function (png) {
                allure.createAttachment('Screenshot', function () {
                    return new Buffer(png, 'base64')
                }, 'image/png')();
                done();
            })


        });

    }
};

