# **_QPP Test_automation Framework_**

Getting Started
---------------
####To get set up and running quickly:

- If you don't have installed [nodejs](https://nodejs.org/dist/v6.9.1/node-v6.9.1.pkg) install it first.
- Check out test framework using SourceTree or Git command line. 
- Install dependencies by running: 
```
npm install -g

- if you didn't already install protractor

npm install -g protractor

- if you didn't already update webdriver 

webdriver-manager update

```
####You can run tests on few different ways:
- Runs tests locally without report.
```
protractor local.conf.js 
```
- Runs tests on Saucelabs without report.
```
protractor saucelabs.conf.js 
```
- You can define either run locally or on Saucelabs inside grunt.js and grunt will generate and ope report after test finish.
```   
 grunt 
```

#### Running Visual Test
- To create reference screenshots
 ```
 backstop reference
```
- To test visual camparisions 
 ```
 backstop test
 ```