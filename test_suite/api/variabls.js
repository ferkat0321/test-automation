/**
 * Created by fuerhaiti on 1/3/17.
 */


module.exports={
        imp0: "https://imp0.qpp.cms.gov/?ACA=PrW9Ccxk49",
        val0: "http://val0-webelb-677044720.us-east-1.elb.amazonaws.com",
        prod: "https://qpp.cms.gov",

        Quality_Measures_List : "/api/v1/quality",
        ACI_Measures_List : "/api/v1/aci",
        IA_List : "/api/v1/ia",
        IA_Filter_Values: "/api/v1/ia_filters",
        Quality_Measures_Filter_Values: "/api/v1/quality_filters",



}