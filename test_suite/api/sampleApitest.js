var chakram = require('chakram');
expect = chakram.expect;

testConfig = require('/Users/fuerhaiti/Desktop/QPPIT/test_suite/api/config.json');

describe("Sample test on QPP uri", function () {
    var apiResponse;
    var result;

    it("Should get response", function () {
        return apiResponse = chakram.get(testConfig.qpp.url + testConfig.qpp.quality_api).then(function (result) {

            var text = result.body.isSuccess;
            var text1 = result.body.serviceData.quality_measures[0].measure_name;
            console.log("This is the testing url: " + testConfig.qpp.url + testConfig.qpp.quality_api);
            console.log("This is the success message: " + text);
            console.log("This is the measure name: " + text1);

        })

    });

    it("should return 200 on success", function () {
        var apiResponse = chakram.get(testConfig.qpp.url + testConfig.qpp.quality_api);
        return expect(apiResponse).to.have.status(200);
    });

});


