var frisby = require('frisby');
var api = require('../../.././test_suite/api/variabls.js');
var imp0=api.imp0;
var val0=api.val0;
var prod=api.prod;

frisby.globalSetup({
    request: {
        headers: {
            'Accept': 'application/json',
            'Cookie': 'ACA=PrW9Ccxk49'
        }
    },
    timeout: (30 * 1000)
});

frisby.create('Improvement Activities Filter values verification.')
    .get("https://imp0.qpp.cms.gov/api/v1/ia_filters")
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
        "isSuccess": true,
        "serviceData": {
            "subcategory_name": [
                "Achieving Health Equity",
                "Behavioral and Mental Health",
                "Beneficiary Engagement",
                "Care Coordination",
                "Emergency Response & Preparedness",
                "Expanded Practice Access",
                "Patient Safety & Practice Assessment",
                "Population Management"
            ],
            "activity_weighting": [
                "Medium",
                "High"
            ]
        }
    })
    .expectJSONTypes(

        {
            "isSuccess": true,
            "serviceData": {
                "subcategory_name": [
                    "string"
                ],
                "activity_weighting": [
                    "string"
                ]
            }
        }



    )
    .toss()

