var frisby = require('frisby');
var api = require('../../.././test_suite/api/variabls.js');
var imp0=api.imp0;
var val0=api.val0;
var prod=api.prod;

frisby.globalSetup({
    request: {
        headers: {
            'Accept': 'application/json',
            'Cookie': 'ACA=PrW9Ccxk49'
        }
    },
    timeout: (30 * 1000)
});

frisby.create('Quality Measure Filter Values Verification.')
    .get("https://imp0.qpp.cms.gov/api/v1/quality_filters")
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .expectJSON({
        "isSuccess": true,
        "serviceData": {
            "specialty_measure_set": [
                "Allergy/ Immunology",
                "Anesthesiology",
                "Cardiology",
                "Dermatology",
                "Diagnostic Radiology",
                "Electro-physiology Cardiac Specialist",
                "Emergency Medicine",
                "Gastro-enterology",
                "General Oncology",
                "General Practice/ Family Medicine",
                "General Surgery",
                "Hospitalists",
                "Internal Medicine",
                "Interventional Radiology",
                "Mental/ Behavioral Health",
                "Neurology",
                "Obstetrics/Gynecology",
                "Ophthalmology",
                "Orthopedic Surgery",
                "Otolaryngology",
                "Pathology",
                "Pediatrics",
                "Physical Medicine",
                "Plastic Surgery",
                "Preventive Medicine",
                "Radiation Oncology",
                "Rheumatology",
                "Thoracic Surgery",
                "Urology",
                "Vascular Surgery"
            ],
            "high_priority_measure": [
                "Yes",
                "No"
            ],
            "data_submission_method": [
                "Administrative Claims",
                "Claims",
                "CSV",
                "CMS Web Interface",
                "EHR",
                "Registry"
            ]
        }

    })
    .expectJSONTypes({
            "isSuccess": true,
            "serviceData": {
                "specialty_measure_set": [
                    "string"
                ],
                "high_priority_measure": [
                    "string"
                ],
                "data_submission_method": [
                    "string"
                ]
            }
        }

    )
    .toss()




