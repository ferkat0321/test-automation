var frisby = require('frisby');
var api = require('../../.././test_suite/api/variabls.js');
var imp0=api.imp0;
var val0=api.val0;
var prod=api.prod;

frisby.globalSetup({
    request: {
        headers: {
            'Accept': 'application/json',
            'Cookie': 'ACA=PrW9Ccxk49'
        }
    },
    timeout: (30 * 1000)
});

frisby.create('Improvement Activities List verification.')
    .get("https://imp0.qpp.cms.gov/api/v1/ia")
    .expectStatus(200)
    .expectJSONLength('serviceData.improvement_activities', 92)
    .expectJSON({
        "isSuccess": true,
        "serviceData": {
            "improvement_activities": [
                {
                    "activity_name": "Additional improvements in access as a result of QIN/QIO TA",
                    "activity_description": "As a result of Quality Innovation Network-Quality Improvement Organization technical assistance, performance of additional activities that improve access to services (e.g., investment of on-site diabetes educator).",
                    "activity_id": "IA_EPA_4",
                    "subcategory_name": "Expanded Practice Access",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Administration of the AHRQ Survey of Patient Safety Culture",
                    "activity_description": "Administration of the AHRQ Survey of Patient Safety Culture and submission of data to the comparative database (refer to AHRQ Survey of Patient Safety Culture website http://www.ahrq.gov/professionals/quality-patient-safety/patientsafetyculture/index.html)",
                    "activity_id": "IA_PSPA_4",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Annual registration in the Prescription Drug Monitoring Program",
                    "activity_description": "Annual registration by eligible clinician or group in the prescription drug monitoring program of the state where they practice. Activities that simply involve registration are not sufficient. MIPS eligible clinicians and  groups must participate for a minimum of 6 months.",
                    "activity_id": "IA_PSPA_5",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Anticoagulant management improvements",
                    "activity_description": "MIPS eligible clinicians and  groups who prescribe oral Vitamin K antagonist therapy (warfarin) must attest that, in the first performance year, 60 percent or more of their ambulatory care patients receiving warfarin are being managed by one or more of these clinical practice improvement activities:  Patients are being managed by an anticoagulant management service, that involves systematic and coordinated care*, incorporating comprehensive patient education, systematic INR testing, tracking, follow-up, and patient communication of results and dosing decisions;  Patients are being managed according to validated electronic decision support and clinical management tools that involve systematic and coordinated care, incorporating comprehensive patient education, systematic INR testing, tracking, follow-up, and patient communication of results and dosing decisions;  For rural or remote patients, patients are managed using remote monitoring or telehealth options that involve systematic and coordinated care, incorporating comprehensive patient education, systematic INR testing, tracking, follow-up, and patient communication of results and dosing decisions; and/or  For patients who demonstrate motivation, competency, and adherence, patients are managed using either a patient self-testing (PST) or patient-self-management (PSM) program.  The performance threshold will increase to 75 percent for the second performance year and onward. Clinicians would attest that, 60 percent for first year, or 75 percent for the second year, of their ambulatory care patients receiving warfarin participated in an anticoagulation management program for at least 90 days during the performance period.",
                    "activity_id": "IA_PM_2",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Care coordination agreements that promote improvements in patient tracking across settings",
                    "activity_description": "Establish effective care coordination and active referral management that could include one or more of the following:  Establish care coordination agreements with frequently used consultants that set expectations for documented flow of information and MIPS eligible clinician or MIPS eligible clinician group expectations between settings. Provide patients with information that sets their expectations consistently with the care coordination agreements;  Track patients referred to specialist through the entire process; and/or Systematically integrate information from referrals into the plan of care.",
                    "activity_id": "IA_CC_12",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Care transition documentation practice improvements",
                    "activity_description": "Implementation of practices/processes for care transition that include documentation of how a MIPS eligible clinician or   group carried out a patient-centered action plan for first 30 days following a discharge (e.g., staff involved, phone calls conducted in support of transition, accompaniments, navigation actions, home visits, patient information access, etc.).",
                    "activity_id": "IA_CC_10",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Care transition standard operational improvements",
                    "activity_description": "Establish standard operations to manage transitions of care that could include one or more of the following:   Establish formalized lines of communication with local settings in which empaneled patients receive care to ensure documented flow of information and seamless transitions in care; and/or  Partner with community or hospital-based transitional care services.",
                    "activity_id": "IA_CC_11",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Chronic care and preventative care management for empanelled patients",
                    "activity_description": "Proactively manage chronic and preventive care for empaneled patients that could include one or more of the following:   Provide patients annually with an opportunity for development and/or adjustment of an individualized plan of care as appropriate to age and health status, including health risk appraisal; gender, age and condition-specific preventive care services; plan of care for chronic conditions; and advance care planning; Use condition-specific pathways for care of chronic conditions (e.g., hypertension, diabetes, depression, asthma and heart failure) with evidence-based protocols to guide treatment to target; Use pre-visit planning to optimize preventive care and team management of patients with chronic conditions; Use panel support tools (registry functionality) to identify services due;  Use reminders and outreach (e.g., phone calls, emails, postcards, patient portals and community health workers where available) to alert and educate patients about services due; and/or Routine medication reconciliation.",
                    "activity_id": "IA_PM_13",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "CMS partner in Patients Hospital Engagement Network",
                    "activity_description": "Membership and participation in a CMS Partnership for Patients Hospital Engagement Network.",
                    "activity_id": "IA_CC_5",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Collection and follow-up on patient experience and satisfaction data on beneficiary engagement",
                    "activity_description": "Collection and follow-up on patient experience and satisfaction data on beneficiary engagement, including development of improvement plan.",
                    "activity_id": "IA_BE_6",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Collection and use of patient experience and satisfaction data on access",
                    "activity_description": "Collection of patient experience and satisfaction data on access to care and development of an improvement plan, such as outlining steps for improving communications with patients to help understanding of urgent access needs.",
                    "activity_id": "IA_EPA_3",
                    "subcategory_name": "Expanded Practice Access",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Completion of the AMA STEPS Forward program",
                    "activity_description": "Completion of the American Medical Association's STEPS Forward program.",
                    "activity_id": "IA_PSPA_9",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Completion of training and receipt of approved waiver for provision opioid medication-assisted treatments",
                    "activity_description": "Completion of training and obtaining an approved waiver for provision of medication-assisted treatment of opioid use disorders using buprenorphine.",
                    "activity_id": "IA_PSPA_10",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Consultation of the Prescription Drug Monitoring program",
                    "activity_description": "Clinicians would attest that, 60 percent for first year, or 75 percent for the second year, of consultation of prescription drug monitoring program prior to the issuance of a Controlled Substance Schedule II (CSII) opioid prescription that lasts for longer than 3 days.",
                    "activity_id": "IA_PSPA_6",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Depression screening",
                    "activity_description": "Depression screening and follow-up plan:  Regular engagement of MIPS eligible clinicians or  groups in integrated prevention and treatment interventions, including depression screening and follow-up plan (refer to NQF #0418) for patients with co-occurring conditions of behavioral or mental health conditions.",
                    "activity_id": "IA_BMH_4",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Diabetes screening",
                    "activity_description": "Diabetes screening for people with schizophrenia or bipolar disease who are using antipsychotic medication.",
                    "activity_id": "IA_BMH_1",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Electronic Health Record Enhancements for BH data capture",
                    "activity_description": "Enhancements to an electronic health record to capture additional data on behavioral health (BH) populations and use that data for additional decision-making purposes (e.g., capture of additional BH data results in additional depression screening for at-risk patient not previously identified).",
                    "activity_id": "IA_BMH_8",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Engagement of community for health status improvement",
                    "activity_description": "Take steps to improve health status of communities, such as collaborating with key partners and stakeholders to implement evidenced-based practices to improve a specific chronic condition.  Refer to the local Quality Improvement Organization (QIO) for additional steps to take for improving health status of communities as there are many steps to select from for satisfying this activity.  QIOs work under the direction of CMS to assist MIPS eligible clinicians and groups with quality improvement, and review quality concerns for the protection of beneficiaries and the Medicare Trust Fund.",
                    "activity_id": "IA_PM_5",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Engagement of new Medicaid patients and follow-up",
                    "activity_description": "Seeing new and follow-up Medicaid patients in a timely manner, including individuals dually eligible for Medicaid and Medicare.",
                    "activity_id": "IA_AHE_1",
                    "subcategory_name": "Achieving Health Equity",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Engagement of patients, family and caregivers in developing a plan of care",
                    "activity_description": "Engage patients, family and caregivers in developing a plan of care and prioritizing their goals for action, documented in the certified EHR technology.",
                    "activity_id": "IA_BE_15",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Engagement of patients through implementation of improvements in patient portal",
                    "activity_description": "Access to an enhanced patient portal that provides up to date information related to relevant chronic disease health or blood pressure control, and includes interactive features allowing patients to enter health information and/or enables bidirectional communication about medication changes and adherence.",
                    "activity_id": "IA_BE_4",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Engagement with QIN-QIO to implement self-management training programs",
                    "activity_description": "Engagement with a Quality Innovation Network-Quality Improvement Organization, which may include participation in self-management training programs such as diabetes.",
                    "activity_id": "IA_BE_3",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Engage patients and families to guide improvement in the system of care.",
                    "activity_description": "Engage patients and families to guide improvement in the system of care.",
                    "activity_id": "IA_BE_14",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Enhancements/regular updates to practice websites/tools that also include considerations for patients with cognitive disabilities",
                    "activity_description": "Enhancements and ongoing regular updates and use of websites/tools that include consideration for compliance with section 508 of the Rehabilitation Act of 1973 or for improved design for patients with cognitive disabilities. Refer to the CMS website on Section 508 of the Rehabilitation Act https://www.cms.gov/Research-Statistics-Data-and-Systems/CMS-Information-Technology/Section508/index.html?redirect=/InfoTechGenInfo/07_Section508.asp that requires that institutions receiving federal funds solicit, procure, maintain and use all electronic and information technology (EIT) so that equal or alternate/comparable access is given to members of the public with and without disabilities.  For example, this includes designing a patient portal or website that is compliant with section 508 of the Rehabilitation Act of 1973",
                    "activity_id": "IA_BE_5",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Evidenced-based techniques to promote self-management into usual care",
                    "activity_description": "Incorporate evidence-based techniques to promote self-management into usual care, using techniques such as goal setting with structured follow-up, Teach Back, action planning or motivational interviewing.",
                    "activity_id": "IA_BE_16",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Glycemic management services",
                    "activity_description": "For outpatient Medicare beneficiaries with diabetes and who are prescribed antidiabetic agents (e.g., insulin, sulfonylureas), MIPS eligible clinicians and groups must attest to having: For the first performance year, at least 60 percent of medical records with documentation of an individualized glycemic treatment goal that: a) Takes into account patient-specific factors, including, at least 1) age, 2) comorbidities, and 3) risk for hypoglycemia, and b) Is reassessed at least annually.  The performance threshold will increase to 75 percent for the second performance year and onward. Clinician would attest that, 60 percent for first year, or 75 percent for the second year, of their medical records that document individualized glycemic treatment represent patients who are being treated for at least 90 days during the performance period.",
                    "activity_id": "IA_PM_4",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Implementation of additional activity as a result of TA for improving care coordination",
                    "activity_description": "Implementation of at least one additional recommended activity from the Quality Innovation Network-Quality Improvement Organization after technical assistance has been provided related to improving care coordination.",
                    "activity_id": "IA_CC_3",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of analytic capabilities to manage total cost of care for practice population",
                    "activity_description": "Build the analytic capability required to manage total cost of care for the practice population that could include one or more of the following:  Train appropriate staff on interpretation of cost and utilization information; and/or  Use available data regularly to analyze opportunities to reduce cost through improved care.",
                    "activity_id": "IA_PSPA_17",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of antibiotic stewardship program",
                    "activity_description": "Implementation of an antibiotic stewardship program that measures the appropriate use of antibiotics for several different conditions (URI Rx in children, diagnosis of pharyngitis, Bronchitis Rx in adults)  according to clinical guidelines for diagnostics and therapeutics",
                    "activity_id": "IA_PSPA_15",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of co-location PCP and MH services",
                    "activity_description": "Integration facilitation, and promotion of the colocation of mental health services in primary and/or non-primary clinical care settings.",
                    "activity_id": "IA_BMH_6",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Implementation of condition-specific chronic disease self-management support programs",
                    "activity_description": "Provide condition-specific chronic disease self-management support programs or coaching or link patients to those programs in the community.",
                    "activity_id": "IA_BE_20",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of documentation improvements for practice/process improvements",
                    "activity_description": "Implementation of practices/processes that document care coordination activities (e.g., a documented care coordination encounter that tracks all clinical staff involved and communications from date patient is scheduled for outpatient procedure through day of procedure).",
                    "activity_id": "IA_CC_8",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of episodic care management practice improvements",
                    "activity_description": "Provide episodic care management, including management across transitions and referrals that could include one or more of the following: Routine and timely follow-up to hospitalizations, ED visits and stays in other institutional settings, including symptom and disease management, and medication reconciliation and management; and/or Managing care intensively through new diagnoses, injuries and exacerbations of illness.",
                    "activity_id": "IA_PM_15",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of fall screening and assessment programs",
                    "activity_description": "Implementation of fall screening and assessment programs to identify patients at risk for falls and address modifiable risk factors (e.g., Clinical decision support/prompts in the electronic health record that help manage the use of medications, such as benzodiazepines, that increase fall risk).",
                    "activity_id": "IA_PSPA_21",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of formal quality improvement methods, practice changes or other practice improvement processes",
                    "activity_description": "Adopt a formal model for quality improvement and create a culture in which all staff actively participates in improvement activities that could include one or more of the following:   Train all staff in quality improvement methods;  Integrate practice change/quality improvement into staff duties; Engage all staff in identifying and testing practices changes; Designate regular team meetings to review data and plan improvement cycles; Promote transparency and accelerate improvement by sharing practice level and panel level quality of care, patient experience and utilization data with staff; and/or Promote transparency and engage patients and families by sharing practice level quality of care, patient experience and utilization data with patients and families.",
                    "activity_id": "IA_PSPA_19",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of improvements that contribute to more timely communication of test results",
                    "activity_description": "Timely communication of test results defined as timely identification of abnormal test results with timely follow-up.",
                    "activity_id": "IA_CC_2",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of integrated PCBH model",
                    "activity_description": "Offer integrated behavioral health services to support patients with behavioral health needs, dementia, and poorly controlled chronic conditions that could include one or more of the following: Use evidence-based treatment protocols and treatment to goal where appropriate; Use evidence-based screening and case finding strategies to identify individuals at risk and in need of services; Ensure regular communication and coordinated workflows between eligible clinicians in primary care and behavioral health; Conduct regular case reviews for at-risk or unstable patients and those who are not responding to treatment; Use of a registry or  certified health information technology  functionality to support active care management and outreach to patients in treatment; and/or Integrate behavioral health and medical care plans and facilitate integration through co-location of services when feasible.",
                    "activity_id": "IA_BMH_7",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Implementation of medication management practice improvements",
                    "activity_description": "Manage medications to maximize efficiency, effectiveness and safety that could include one or more of the following:  Reconcile and coordinate medications and provide medication management across transitions of care settings and eligible clinicians or  groups;  Integrate a pharmacist into the care team; and/or Conduct periodic, structured medication reviews.",
                    "activity_id": "IA_PM_16",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of methodologies for improvements in longitudinal care management for high risk patients",
                    "activity_description": "Provide longitudinal care management to patients at high risk for adverse health outcome or harm that could include one or more of the following: Use a consistent method to assign and adjust global risk status for all empaneled patients to allow risk stratification into actionable risk cohorts. Monitor the risk-stratification method and refine as necessary to improve accuracy of risk status identification; Use a personalized plan of care for patients at high risk for adverse health outcome or harm, integrating patient goals, values and priorities; and/or Use on-site practice-based or shared care managers to proactively monitor and coordinate care for the highest risk cohort of patients.",
                    "activity_id": "IA_PM_14",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of practices/processes for developing regular individual care plans",
                    "activity_description": "Implementation of practices/processes to develop regularly updated individual care plans for at-risk patients that are shared with the beneficiary or caregiver(s).",
                    "activity_id": "IA_CC_9",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Implementation of use of specialist reports back to referring clinician or group to close referral loop",
                    "activity_description": "Performance of regular practices that include providing specialist reports back to the referring MIPS eligible clinician or  group to close the referral loop or where the referring MIPS eligible clinician or   group initiates regular inquiries to specialist for specialist reports which could be documented or noted in the certified EHR technology.",
                    "activity_id": "IA_CC_1",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Improved practices that disseminate appropriate self-management materials",
                    "activity_description": "Provide self-management materials at an appropriate literacy level and in an appropriate language.",
                    "activity_id": "IA_BE_21",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Improved practices that engage patients pre-visit",
                    "activity_description": "Provide a pre-visit development of a shared visit agenda with the patient.",
                    "activity_id": "IA_BE_22",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Integration of patient coaching practices between visits",
                    "activity_description": "Provide coaching between visits with follow-up on care plan and goals.",
                    "activity_id": "IA_BE_23",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Leadership engagement in regular guidance and demonstrated commitment for implementing practice improvement changes",
                    "activity_description": "Ensure full engagement of clinical and administrative leadership in practice improvement that could include one or more of the following:     Make responsibility for guidance of practice change a component of clinical and administrative leadership roles;   Allocate time for clinical and administrative leadership for practice improvement efforts, including participation in regular team meetings; and/or  Incorporate population health, quality and patient experience metrics in regular reviews of practice performance.",
                    "activity_id": "IA_PSPA_20",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Leveraging a QCDR for use of standard questionnaires",
                    "activity_description": "Participation in a QCDR, demonstrating performance of activities for use of standard questionnaires for assessing improvements in health disparities related to functional health status (e.g., use of Seattle Angina Questionnaire, MD Anderson Symptom Inventory, and/or SF-12/VR-12 functional health status assessment).",
                    "activity_id": "IA_AHE_4",
                    "subcategory_name": "Achieving Health Equity",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Leveraging a QCDR to promote use of patient-reported outcome tools",
                    "activity_description": "Participation in a QCDR, demonstrating performance of activities for promoting use of patient-reported outcome (PRO) tools and corresponding collection of PRO data (e.g., use of PQH-2 or PHQ-9 and PROMIS instruments).",
                    "activity_id": "IA_AHE_3",
                    "subcategory_name": "Achieving Health Equity",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Leveraging a QCDR to standardize processes for screening",
                    "activity_description": "Participation in a QCDR, demonstrating performance of activities for use of standardized processes for screening for social determinants of health such as food security, employment and housing.  Use of supporting tools that can be incorporated into the certified EHR technology is also suggested.",
                    "activity_id": "IA_AHE_2",
                    "subcategory_name": "Achieving Health Equity",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "MDD prevention and treatment interventions",
                    "activity_description": "Major depressive disorder: Regular engagement of MIPS eligible clinicians or  groups in integrated prevention and treatment interventions, including suicide risk assessment (refer to NQF #0104) for mental health patients with co-occurring conditions of behavioral or mental health conditions.",
                    "activity_id": "IA_BMH_5",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Measurement and improvement at the practice and panel level",
                    "activity_description": "Measure and improve quality at the practice and panel level that could include one or more of the following:  Regularly review measures of quality, utilization, patient satisfaction and other measures that may be useful at the practice level and at the level of the care team or MIPS eligible clinician or group(panel); and/or Use relevant data sources to create benchmarks and goals for performance at the practice level and panel level.",
                    "activity_id": "IA_PSPA_18",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participate in IHI Training/Forum Event; National Academy of Medicine, AHRQ Team STEPPS(R) or other similar activity.",
                    "activity_description": "For eligible professionals not participating in Maintenance of Certification (MOC) Part IV, new engagement for MOC Part IV, such as IHI Training/Forum Event; National Academy of Medicine, AHRQ Team STEPPS(R)",
                    "activity_id": "IA_PSPA_3",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in a 60-day or greater effort to support domestic or international humanitarian needs.",
                    "activity_description": "Participation in domestic or international humanitarian volunteer work. Activities that simply involve registration are not sufficient.  MIPS eligible clinicians attest to domestic or international humanitarian volunteer work for a period of a continuous 60 days or greater.",
                    "activity_id": "IA_ERP_2",
                    "subcategory_name": "Emergency Response & Preparedness",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Participation in an AHRQ-listed patient safety organization.",
                    "activity_description": "Participation in an AHRQ-listed patient safety organization.",
                    "activity_id": "IA_PSPA_1",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in a QCDR, that promotes collaborative learning network opportunities that are interactive.",
                    "activity_description": "Participation in a QCDR, that promotes collaborative learning network opportunities that are interactive.",
                    "activity_id": "IA_BE_8",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in a QCDR, that promotes implementation of patient self-action plans.",
                    "activity_description": "Participation in a QCDR, that promotes implementation of patient self-action plans.",
                    "activity_id": "IA_BE_10",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in a QCDR, that promotes use of patient engagement tools.",
                    "activity_description": "Participation in a QCDR, that promotes use of patient engagement tools.",
                    "activity_id": "IA_BE_7",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in a QCDR, that promotes use of processes and tools that engage patients for adherence to treatment plan.",
                    "activity_description": "Participation in a QCDR, that promotes use of processes and tools that engage patients for adherence to treatment plan.",
                    "activity_id": "IA_BE_11",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in Bridges to Excellence or other similar program",
                    "activity_description": "Participation in other quality improvement programs such as Bridges to Excellence",
                    "activity_id": "IA_PSPA_14",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in CAHPS or other supplemental questionnaire",
                    "activity_description": "Participation in the Consumer Assessment of Healthcare Providers and Systems Survey or other supplemental questionnaire items (e.g., Cultural Competence or Health Information Technology supplemental item sets).",
                    "activity_id": "IA_PSPA_11",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Participation in CMMI models such as Million Hearts Campaign",
                    "activity_description": "Participation in CMMI models such as the Million Hearts Cardiovascular Risk Reduction Model",
                    "activity_id": "IA_PM_8",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in Joint Commission Evaluation Initiative",
                    "activity_description": "Participation in Joint Commission Ongoing Professional Practice Evaluation initiative",
                    "activity_id": "IA_PSPA_13",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in MOC Part IV",
                    "activity_description": "Participation in Maintenance of Certification (MOC) Part IV for improving professional practice including participation in a local, regional or national outcomes registry or quality assessment program. Performance of monthly activities across practice to regularly assess performance in practice, by reviewing outcomes addressing identified areas for improvement and evaluating the results.",
                    "activity_id": "IA_PSPA_2",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in population health research",
                    "activity_description": "Participation in research that identifies interventions, tools or processes that can improve a targeted patient population.",
                    "activity_id": "IA_PM_9",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in private payer CPIA",
                    "activity_description": "Participation in designated private payer clinical practice improvement activities.",
                    "activity_id": "IA_PSPA_12",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Participation in systematic anticoagulation program",
                    "activity_description": "Participation in a systematic anticoagulation program (coagulation clinic, patient self-reporting program, patient self-management program)for 60 percent of practice patients in year 1 and 75 percent of practice patients in year 2 who receive anti-coagulation medications (warfarin or other coagulation cascade inhibitors).",
                    "activity_id": "IA_PM_1",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Participation on Disaster Medical Assistance Team, registered for 6 months.",
                    "activity_description": "Participation in Disaster Medical Assistance Teams, or Community Emergency Responder Teams. Activities that simply involve registration are not sufficient.  MIPS eligible clinicians and MIPS eligible clinician groups must be registered for a minimum of 6 months as a volunteer for disaster or emergency response.",
                    "activity_id": "IA_ERP_1",
                    "subcategory_name": "Emergency Response & Preparedness",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Population empanelment",
                    "activity_description": "Empanel (assign responsibility for) the total population, linking each patient to a MIPS eligible clinician or group or care team.  Empanelment is a series of processes that assign each active patient to a MIPS eligible clinician or group and/or care team, confirm assignment with patients and clinicians, and use the resultant patient panels as a foundation for individual patient and population health management.   Empanelment identifies the patients and population for whom the MIPS eligible clinician or group and/or care team is responsible and is the foundation for the relationship continuity between patient and MIPS eligible clinician or group /care team that is at the heart of comprehensive primary care. Effective empanelment requires identification of the \"active population\" of the practice: those patients who identify and use your practice as a source for primary care. There are many ways to define \"active patients\" operationally, but generally, the definition of \"active patients\" includes patients who have sought care within the last 24 to 36 months, allowing inclusion of younger patients who have minimal acute or preventive health care.",
                    "activity_id": "IA_PM_12",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Practice improvements for bilateral exchange of patient information",
                    "activity_description": "Ensure that there is bilateral exchange of necessary patient information to guide patient care that could include one or more of the following:   Participate in a Health Information Exchange if available; and/or  Use structured referral notes.",
                    "activity_id": "IA_CC_13",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Practice improvements that engage community resources to support patient health goals",
                    "activity_description": "Develop pathways to neighborhood/community-based resources to support patient health goals that could include one or more of the following:   Maintain formal (referral) links to community-based chronic disease self-management support programs, exercise programs and other wellness resources with the potential for bidirectional flow of information; and/or  Provide a guide to available community resources.",
                    "activity_id": "IA_CC_14",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Provide 24/7 access to eligible clinicians or groups who have  real-time access to patient's medical record",
                    "activity_description": "Provide 24/7 access to MIPS eligible clinicians,  groups, or care teams for advice about urgent and emergent care (e.g., eligible clinician and care team access to medical record, cross-coverage with access to medical record, or protocol-driven nurse line with access to medical record) that could include one or more of the following: Expanded hours in evenings and weekends with access to the patient medical record (e.g., coordinate with small practices to provide alternate hour office visits and urgent care);  Use of alternatives to increase access to care team by MIPS eligible clinicians and  groups, such as e-visits, phone visits, group visits, home visits and alternate locations (e.g., senior centers and assisted living centers); and/or  Provision of same-day or next-day access to a consistent MIPS eligible clinician, group or care team when needed for urgent care or transition management",
                    "activity_id": "IA_EPA_1",
                    "subcategory_name": "Expanded Practice Access",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Provide peer-led support for self-management.",
                    "activity_description": "Provide peer-led support for self-management.",
                    "activity_id": "IA_BE_18",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Regularly assess the patient experience of care through surveys, advisory councils and/or other mechanisms.",
                    "activity_description": "Regularly assess the patient experience of care through surveys, advisory councils and/or other mechanisms.",
                    "activity_id": "IA_BE_13",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Regular review practices in place on targeted patient population needs",
                    "activity_description": "Implementation of regular reviews of targeted patient population needs which includes access to reports that show unique characteristics of eligible professional's patient population, identification of vulnerable patients, and how clinical treatment needs are being tailored, if necessary, to address unique needs and what resources in the community have been identified as additional resources.",
                    "activity_id": "IA_PM_11",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Regular training in care coordination",
                    "activity_description": "Implementation of regular care coordination training.",
                    "activity_id": "IA_CC_7",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "RHC, IHS or FQHC quality improvement activities",
                    "activity_description": "Participating in a Rural Health Clinic (RHC), Indian Health Service Medium Management (IHS), or Federally Qualified Health Center in ongoing engagement activities that contribute to more formal quality reporting in line with Section 1848(q)(2)(B)(iii) of the Act that requires the Secretary to give consideration to the circumstances of practices located in rural areas and geographic HPSAs.  Rural Health Clinics would be included in that definition for consideration of practices in rural areas.",
                    "activity_id": "IA_PM_3",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "TCPI participation",
                    "activity_description": "Participation in the CMS Transforming Clinical Practice Initiative.",
                    "activity_id": "IA_CC_4",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Tobacco use",
                    "activity_description": "Tobacco use: Regular engagement of MIPS eligible clinicians or  groups in integrated prevention and treatment interventions, including tobacco use screening and cessation interventions (refer to NQF #0028) for patients with co-occurring conditions of behavioral or mental health and at risk factors for tobacco dependence.",
                    "activity_id": "IA_BMH_2",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Unhealthy alcohol use",
                    "activity_description": "Unhealthy alcohol use: Regular engagement of MIPS eligible clinicians or  groups in integrated prevention and treatment interventions, including screening and brief counseling (refer to NQF #2152) for patients with co-occurring conditions of behavioral or mental health conditions.",
                    "activity_id": "IA_BMH_3",
                    "subcategory_name": "Behavioral and Mental Health",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use evidence-based decision aids to support shared decision-making.",
                    "activity_description": "Use evidence-based decision aids to support shared decision-making.",
                    "activity_id": "IA_BE_12",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use group visits for common chronic conditions (e.g., diabetes).",
                    "activity_description": "Use group visits for common chronic conditions (e.g., diabetes).",
                    "activity_id": "IA_BE_19",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of certified EHR to capture patient reported outcomes",
                    "activity_description": "In support of improving patient access, performing additional activities that enable capture of patient reported outcomes (e.g., home blood pressure, blood glucose logs, food diaries, at-risk health factors such as tobacco or alcohol use, etc.) or patient activation measures through use of certified EHR technology, containing this data in a separate queue for clinician recognition and review.",
                    "activity_id": "IA_BE_1",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of decision support and standardized treatment protocols",
                    "activity_description": "Use decision support and standardized treatment protocols to manage workflow in the team to meet patient needs.",
                    "activity_id": "IA_PSPA_16",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of patient safety tools",
                    "activity_description": "Use of tools that assist specialty practices in tracking specific measures that are meaningful to their practice, such as use of the Surgical Risk Calculator.",
                    "activity_id": "IA_PSPA_8",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of QCDR data for ongoing practice assessment and improvements",
                    "activity_description": "Use of QCDR data, for ongoing practice assessment and improvements in patient safety.",
                    "activity_id": "IA_PSPA_7",
                    "subcategory_name": "Patient Safety & Practice Assessment",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of QCDR data for quality improvement such as comparative analysis reports across patient populations",
                    "activity_description": "Participation in a QCDR, clinical data registries, or other registries run by other government agencies such as FDA, or private entities such as a hospital or medical or surgical society.  Activity must include use of QCDR data for quality improvement (e.g., comparative analysis across specific patient populations for adverse outcomes after an outpatient surgical procedure and corrective steps to address adverse outcome).",
                    "activity_id": "IA_PM_10",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of QCDR for feedback reports that incorporate population health",
                    "activity_description": "Use of a QCDR to generate regular feedback reports that summarize local practice patterns and treatment outcomes, including for vulnerable populations.",
                    "activity_id": "IA_PM_7",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "High"
                },
                {
                    "activity_name": "Use of QCDR patient experience data to inform and advance improvements in beneficiary engagement.",
                    "activity_description": "Use of QCDR patient experience data to inform and advance improvements in beneficiary engagement.",
                    "activity_id": "IA_BE_9",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of QCDR to promote standard practices, tools and processes in practice for improvement in care coordination",
                    "activity_description": "Participation in a Qualified Clinical Data Registry, demonstrating performance of activities that promote use of standard practices, tools and processes for quality improvement (e.g., documented preventative screening and vaccinations that can be shared across MIPS eligible clinician or groups).",
                    "activity_id": "IA_CC_6",
                    "subcategory_name": "Care Coordination",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of QCDR to support clinical decision making",
                    "activity_description": "Participation in a QCDR, demonstrating performance of activities that promote implementation of shared clinical decision making capabilities.",
                    "activity_id": "IA_BE_2",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of telehealth services that expand practice access",
                    "activity_description": "Use of telehealth services and analysis of data for quality improvement, such as participation in remote specialty care consults or teleaudiology pilots that assess ability to still deliver quality care to patients.",
                    "activity_id": "IA_EPA_2",
                    "subcategory_name": "Expanded Practice Access",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of toolsets or other resources to close healthcare disparities across communities",
                    "activity_description": "Take steps to improve healthcare disparities, such as Population Health Toolkit or other resources identified by CMS, the Learning and Action Network, Quality Innovation Network, or National Coordinating Center. Refer to the local Quality Improvement Organization (QIO) for additional steps to take for improving health status of communities as there are many steps to select from for satisfying this activity. QIOs work under the direction of CMS to assist eligible clinicians and  groups with quality improvement, and review quality concerns for the protection of beneficiaries and the Medicare Trust Fund.",
                    "activity_id": "IA_PM_6",
                    "subcategory_name": "Population Management",
                    "activity_weighting": "Medium"
                },
                {
                    "activity_name": "Use of tools to assist patient self-management",
                    "activity_description": "Use tools to assist patients in assessing their need for support for self-management (e.g., the Patient Activation Measure or How's My Health).",
                    "activity_id": "IA_BE_17",
                    "subcategory_name": "Beneficiary Engagement",
                    "activity_weighting": "Medium"
                }
            ]
        }
    })
    .expectJSONTypes({
        "isSuccess": true,
        "serviceData": {
            "improvement_activities": [
                {
                    "activity_name": "string",
                    "activity_description": "string",
                    "activity_id": "string",
                    "subcategory_name": "string",
                    "activity_weighting": "string"
                }
            ]
        }
    }
    ).toss()




