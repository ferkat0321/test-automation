/**
 * Created by Ferkat on 3/20/17.
 */
var frisby = require('frisby');

var cityName='Baltimore';
function apiTest() {
    var response= frisby.create('Sample api testing on weather api')
        .get('api.openweathermap.org/data/2.5/weather?q='+cityName)
        .toss();
    console.log(response);
}