var frisby = require('frisby');
var api = require('../../.././test_suite/api/variabls.js');
var imp0=api.imp0;
var val0=api.val0;
var prod=api.prod;

frisby.create('ACI measure list api verification')
.get(val0+api.ACI_Measures_List)
.expectStatus(200)
.expectHeaderContains('content-type','application/json')
.expectJSONLength('serviceData.aci_measures.aci_stage_name_1_measures',15)
    .expectJSON({
        "isSuccess": true,
        "serviceData": {
            "aci_measures": {
                "aci_stage_name_1_measures": [
                    {
                        "measure_name": "Security Risk Analysis",
                        "measure_description": "Conduct or review a security risk analysis in accordance with the requirements in 45 CFR 164.308(a)(1), including addressing the security (to include encryption) of ePHI data created or maintained by certified EHR technology in accordance with requirements in 45 CFR164.312(a)(2)(iv) and 45 CFR 164.306(d)(3), and implement security updates as necessary and correct identified security deficiencies as part of the MIPS eligible clinician's risk management process.",
                        "measure_id": "ACI_PPHI_1",
                        "objective_name": "Protect Patient Health Information",
                        "performance_score_weight": "0",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "e-Prescribing",
                        "measure_description": "At least one permissible prescription written by the MIPS eligible clinician is queried for a drug formulary and transmitted electronically using certified EHR technology.",
                        "measure_id": "ACI_EP_1",
                        "objective_name": "Electronic Prescribing",
                        "performance_score_weight": "0",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "Provide Patient Access",
                        "measure_description": "For at least one unique patient seen by the MIPS eligible clinician: (1) The patient (or the patient authorized representative) is provided timely access to view online, download, and transmit his or her health information; and (2) The MIPS eligible clinician ensures the patient's health information is available for the patient (or patient-authorized representative) to access using any application of their choice that is configured to meet the technical specifications of the Application Programing Interface (API) in the MIPS eligible clinician's certified EHR technology.",
                        "measure_id": "ACI_PEA_1",
                        "objective_name": "Patient Electronic Access",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "Patient-Specific Education",
                        "measure_description": "The MIPS eligible clinician must use clinically relevant information from certified EHR technology to identify patient-specific educational resources and provide electronic access to those materials to at least one unique patient seen by the MIPS eligible clinician.",
                        "measure_id": "ACI_PEA_2",
                        "objective_name": "Patient Electronic Access",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "View, Download and Transmit (VDT)",
                        "measure_description": "During the performance period, at least one unique patient (or patient-authorized representatives) seen by the MIPS eligible clinician actively engages with the EHR made accessible by the MIPS eligible clinician. An MIPS eligible clinician may meet the measure by either-(1) view, download or transmit to a third party their health information; or (2) access their health information through the use of an API that can be used by applications chosen by the patient and configured to the API in the MIPS eligible clinician's certified EHR technology; or (3) a combination of (1) and (2).",
                        "measure_id": "ACI_CCTPE_1",
                        "objective_name": "Coordination of Care Through Patient Engagement",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Secure Messaging",
                        "measure_description": "For at least one unique patient seen by the MIPS eligible clinician during the performance period, a secure message was sent using the electronic messaging function of certified EHR technology to the patient (or the patient-authorized representative), or in response to a secure message sent by the patient (or the patient-authorized representative), or in response to a secure message sent by the patient (or the patient-authorized representative).",
                        "measure_id": "ACI_CCTPE_2",
                        "objective_name": "Coordination of Care Through Patient Engagement",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Patient-Generated Health Data",
                        "measure_description": "Patient-generated health data or data from a non-clinical setting is incorporated into the certified EHR technology for at least one unique patient seen by the MIPS eligible clinician during the performance period.",
                        "measure_id": "ACI_CCTPE_3",
                        "objective_name": "Coordination of Care Through Patient Engagement",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Send a Summary of Care",
                        "measure_description": "For at least one transition of care or referral, the MIPS eligible clinician that transitions or refers their patient to another setting of care or health care provider-(1) creates a summary of care record using certified EHR technology; and (2) electronically exchanges the summary of care record.",
                        "measure_id": "ACI_HIE_1",
                        "objective_name": "Health Information Exchange",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "Request/Accept Summary of Care",
                        "measure_description": "For at least one transition of care or referral received or patient encounter in which the MIPS eligible clinician has never before encountered the patient, the MIPS eligible clinician receives or retrieves and incorporates into the patient's record an electronic summary of care document.",
                        "measure_id": "ACI_HIE_2",
                        "objective_name": "Health Information Exchange",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "Clinical Information Reconciliation",
                        "measure_description": "For at least one transition of care or referral received or patient encounter in which the MIPS eligible clinician has never before encountered the patient, the MIPS eligible clinician performs clinical information reconciliation. The MIPS eligible clinician must implement clinical information reconciliation for the following three clinical information sets: (1) Medication. Review of the patient's medication, including the name, dosage, frequency, and route of each medication. (2) Medication allergy. Review of the patient's known medication allergies. (3) Current Problem list. Review of the patient's current and active diagnoses.",
                        "measure_id": "ACI_HIE_3",
                        "objective_name": "Health Information Exchange",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Immunization Registry Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement with a public health agency to submit immunization data and receive immunization forecasts and histories from the public health immunization registry/immunization information system (IIS).",
                        "measure_id": "ACI_PHCDRR_1",
                        "objective_name": "Public Health and Clinical Data Registry Reporting",
                        "performance_score_weight": "0 or 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Syndromic Surveillance Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement with a public health agency to submit syndromic surveillance data from a urgent care ambulatory setting where the jurisdiction accepts syndromic data from such settings and the standards are clearly defined.  Earn a 5 % bonus in the advancing care information performance category score for submitting to one or more public health or clinical data registries.",
                        "measure_id": "ACI_PHCDRR_2",
                        "objective_name": "Public Health and Clinical Data Registry Reporting",
                        "performance_score_weight": "0",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Electronic Case Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement with a public health agency to electronically submit case reporting of reportable conditions.  Earn a 5 % bonus in the advancing care information performance category score for submitting to one or more public health or clinical data registries.",
                        "measure_id": "ACI_PHCDRR_3",
                        "objective_name": "Public Health and Clinical Data Registry Reporting",
                        "performance_score_weight": "0",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Public Health Registry Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement with a public health agency to submit data to public health registries.  Earn a 5 % bonus in the advancing care information performance category score for submitting to one or more public health or clinical data registries.",
                        "measure_id": "ACI_PHCDRR_4",
                        "objective_name": "Public Health and Clinical Data Registry Reporting",
                        "performance_score_weight": "0",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Clinical Data Registry Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement to submit data to a clinical data registry.  Earn a 5 % bonus in the advancing care information performance category score for submitting to one or more public health or clinical data registries.",
                        "measure_id": "ACI_PHCDRR_5",
                        "objective_name": "Public Health and Clinical Data Registry Reporting",
                        "performance_score_weight": "0",
                        "required_for_base_score": "No"
                    }
                ],
                "aci_stage_name_2_measures": [
                    {
                        "measure_name": "Security Risk Analysis",
                        "measure_description": "Conduct or review a security risk analysis in accordance with the requirements in 45 CFR 164.308(a)(1), including addressing the security (to include encryption) of ePHI data created or maintained by certified EHR technology in accordance with requirements in 45 CFR164.312(a)(2)(iv) and 45 CFR 164.306(d)(3), and implement security updates as necessary and correct identified security deficiencies as part of the MIPS eligible clinician's risk management process.",
                        "measure_id": "ACI_TRANS_PPHI_1",
                        "objective_name": "Protect Patient Health Information",
                        "performance_score_weight": "0",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "e-Prescribing",
                        "measure_description": "At least one permissible prescription written by the MIPS eligible clinician is queried for a drug formulary and transmitted electronically using certified EHR technology.",
                        "measure_id": "ACI_TRANS_EP_1",
                        "objective_name": "Electronic Prescribing",
                        "performance_score_weight": "0",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "Provide Patient Access",
                        "measure_description": "At least one patient seen by the MIPS eligible clinician during the performance period is provided timely access to view online, download, and transmit to a third party their health information subject to the MIPS eligible clinician's discretion to withhold certain information.",
                        "measure_id": "ACI_TRANS_PEA_1",
                        "objective_name": "Patient Electronic Access",
                        "performance_score_weight": "Up to 20%",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "View, Download, or Transmit (VDT)",
                        "measure_description": "At least one patient seen by the MIPS eligible clinician during the performance period (or patient-authorized representative) views, downloads or transmits their health information to a third party during the performance period.",
                        "measure_id": "ACI_TRANS_PEA_2",
                        "objective_name": "Patient Electronic Access",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Secure Messaging",
                        "measure_description": "For at least one unique patient seen by the MIPS eligible clinician during the performance period, a secure message was sent using the electronic messaging function of CEHRT to the patient (or the patient-authorized representative), or in response to a secure message sent by the patient (or the patient-authorized representative) during the performance period.",
                        "measure_id": "ACI_TRANS_SM_1",
                        "objective_name": "Secure Messaging",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Health Information Exchange",
                        "measure_description": "The MIPS eligible clinician that transitions or refers their patient to another setting of care or health care clinician (1) uses CEHRT to create a summary of care record; and (2) electronically transmits such summary to a receiving health care clinician for at least one transition of care or referral.",
                        "measure_id": "ACI_TRANS_HIE_1",
                        "objective_name": "Health Information Exchange",
                        "performance_score_weight": "Up to 20%",
                        "required_for_base_score": "Yes"
                    },
                    {
                        "measure_name": "Medication Reconciliation",
                        "measure_description": "The MIPS eligible clinician performs medication reconciliation for at least one transition of care in which the patient is transitioned into the care of the MIPS eligible clinician.",
                        "measure_id": "ACI_TRANS_MR_1",
                        "objective_name": "Medication Reconciliation",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Immunization Registry Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement with a public health agency to submit immunization data.",
                        "measure_id": "ACI_TRANS_PHCDRR_1",
                        "objective_name": "Public Health Reporting",
                        "performance_score_weight": "0 or 10%",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Syndromic Surveillance Reporting",
                        "measure_description": "The MIPS eligible clinician  is in active engagement with a public health agency to submit syndromic surveillance data.  Earn a 5 % bonus in the advancing care information performance category score for submitting to one or more public health or clinical data registries.",
                        "measure_id": "ACI_TRANS_PHCDRR_2",
                        "objective_name": "Public Health Reporting",
                        "performance_score_weight": "0",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Specialized Registry Reporting",
                        "measure_description": "The MIPS eligible clinician is in active engagement to submit data to specialized registry.  Earn a 5 % bonus in the advancing care information performance category score for submitting to one or more public health or clinical data registries.",
                        "measure_id": "ACI_TRANS_PHCDRR_3",
                        "objective_name": "Public Health Reporting",
                        "performance_score_weight": "0",
                        "required_for_base_score": "No"
                    },
                    {
                        "measure_name": "Patient-Specific Education",
                        "measure_description": "The MIPS eligible clinician must use clinically relevant information from CEHRT to identify patient-specific educational resources and provide access to those materials to at least one unique patient seen by the MIPS eligible clinician.",
                        "measure_id": "ACI_TRANS_PSE_1",
                        "objective_name": "Patient-Specific Education",
                        "performance_score_weight": "Up to 10%",
                        "required_for_base_score": "No"
                    }
                ]
            }
        }
    })
    .toss()



