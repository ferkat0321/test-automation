/**
 * Created by fuerhaiti on 01/30/2017.
 */

var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var elements = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var elm = require('../../../../.././test_suite/e2e/pages/SignIn.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var variables = require('../../../../.././test_suite/e2e/specs/sprint_test/variables.js');


describe('Boundary for Username and password', function () {

    beforeEach(function () {
        I.amOnPage("/secure/login");
    }),

        it('Verify system will not accept more than 50 chars in user name field.', function () {

            var longUserName = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxy';
            var shortUserName = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwx';
            I.type(elm.SignInPage.UserName, longUserName);
            elm.SignInPage.UserName.getAttribute('value').then(function (value) {
                console.log(value);
                expect(value).toEqual(shortUserName);
            });

        });

    it('Verify system will not accept more than 12 chars in password field.', function () {

        var longPassword = 'ABCDEFGHIJKLMN';
        var shortPassword = 'ABCDEFGHIJKL';
        I.type(elm.SignInPage.Password, longPassword);
        elm.SignInPage.Password.getAttribute('value').then(function (value) {
            console.log(value);
            expect(value).toEqual(shortPassword);
        });

    });


});

