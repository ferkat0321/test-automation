/**
 * Created by fuerhaiti on 12/22/16.
 */

var I = require('../../../helpers/methodLibrary.js');
var elm = require('../../../pages/SignIn.js');
var pageTitles = require('../../../pages/pageTitles.js');
var variables = require('../variables.js');


describe('Forgot User Id.', function () {
    var userId;

    beforeEach(function () {
        I.amOnPage("/secure/login");
    }),

        it('As a QPP user, I need to reset my password using Forgot password link without answering any security question', function () {
            I.seeInTitle(pageTitles.signInPage.SignInPage);
            I.click(elm.SignInPage.ForgotUserId);
            I.seeInTitle(pageTitles.signInPage.ForgotUserId);
            I.type(elm.ForgotUserIdPage.emailTextBox, variables.validData.validEmail);
            I.click(elm.ForgotUserIdPage.sendUserId);

            protractor.promise.controlFlow().execute(function (getLastEmail) {

                var deferred = protractor.promise.defer();
                console.log("Waiting for an email, please be patient...");

                mailListener.on("mail", function (mail) {
                    deferred.fulfill(mail);
                    var json = JSON.stringify(mail);
                    var obj = json;

                    var String = JSON.parse(json).html.replace(/\s/g, '');

                    userId = String.substring(62, 71);

                });

                mailListener.on("error", function (err) {
                    console.log(err);
                });

                return deferred.promise;

            }).then(function (email) {
                expect(email.subject).toEqual('Your I & A user ID');
                expect(email.headers.to).toEqual('<tistaqpp@hotmail.com>');

            });

        });

    it('Navigate back to log in page and log in with user id extracted from email.', function () {

        I.seeInTitle(pageTitles.signInPage.SignInPage);
        I.type(elm.SignInPage.UserName, userId);
        I.type(elm.SignInPage.Password, variables.validData.validPassowrd);
        I.click(elm.SignInPage.LoginButton);
        I.seeInTitle(pageTitles.signInPage.SecurityCode);

    })


});

