/**
 * Created by fuerhaiti on 12/22/16.
 */
var I = require('../../../helpers/methodLibrary.js');
var elm = require('../../../pages/SignIn.js');
var pageTitles = require('../../../pages/pageTitles.js');
var variables = require('../variables.js');


describe('Forgot Password.', function () {

    var changePasswordUrl;

    // beforeEach(function () {
    // }),

    it('As a QPP user, I need to reset my password using Forgot password link without answering any security question', function () {
        I.amOnPage("http://val0-webelb-677044720.us-east-1.elb.amazonaws.com//secure/login");
        I.seeInTitle(pageTitles.signInPage.SignInPage);
        I.click(elm.ForgotUserIdPage.ForgotPassword);
        I.seeInTitle(pageTitles.signInPage.ForgotPassword);
        I.type(elm.ForgotUserIdPage.userId, variables.validData.validUserName);
        I.click(elm.ForgotUserIdPage.sendPasswordButton);


        protractor.promise.controlFlow().execute(function (getLastEmail) {

            var deferred = protractor.promise.defer();
            console.log("Waiting for an email, please be patient...");

            mailListener.on("mail", function (mail) {
                deferred.fulfill(mail);
                var json = JSON.stringify(mail);
                var obj = json;

                var String = JSON.parse(json).html.replace(/\s/g, '');
                //console.log("@@@"+String+"@@@");

                changePasswordUrl = String.substring(169, 264);

                console.log("***" + changePasswordUrl + "***");

            });

            mailListener.on("error", function (err) {
                console.log(err);
            });

            return deferred.promise;

        }).then(function (email) {
            expect(email.subject).toEqual('Reset your password');
            expect(email.headers.to).toEqual('<tistaqpp@hotmail.com>');

        });
    });

    it('Navigate to reset password page.', function () {
        var finalUrl = changePasswordUrl;
        console.log("Final url is: " + finalUrl);

        browser.get(finalUrl);
        //I.amOnPage(finalUrl);
        I.type(elm.ChangeYourPasswordLinkPage.newPasswordBox, variables.validNewData.newPassword);
        I.type(elm.ChangeYourPasswordLinkPage.confirmPasswordBox, variables.validNewData.newPassword);
        I.click(elm.ChangeYourPasswordLinkPage.changePasswordButton);
    })

});


