
Feature('Verify login page.');

Before((I) => {


    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});

Scenario('This scenario verifies error message when user clicks on log in without entering user name and password', function (I, header_footer,SignIn,variables) {

    I.see('Sign In', header_footer.pageHeader.SignIn);
    I.click(header_footer.pageHeader.SignIn);
    I.seeInTitle('Login - Quality Payment Program');
    I.see('Login',SignIn.pageHeaderNavLink.Login);
    I.click(SignIn.LoginPage.LoginButton);

});
