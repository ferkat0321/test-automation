
Feature('Verify login page.');

Before((I) => {


    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});

Scenario('This scenario verifies error message when user tries to log in with invalid user name and valid password.', function (I, header_footer,SignIn,variables) {

    I.see('Sign In', header_footer.pageHeader.SignIn);
    I.click(header_footer.pageHeader.SignIn);
    I.seeInTitle('Login - Quality Payment Program');
    I.see('Login',SignIn.pageHeaderNavLink.Login);
    I.fillField(SignIn.LoginPage.UserName,variables.invalidData.invalidUserName);
    I.fillField(SignIn.LoginPage.Password,variables.validData.validPassowrd);
    I.click(SignIn.LoginPage.LoginButton);
    I.waitForElement(SignIn.LoginPage.ErrorMessage);
    I.see('Error in Get User',SignIn.LoginPage.ErrorMessage);



});
