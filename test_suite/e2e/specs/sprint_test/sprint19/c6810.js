/**
 * Created by fuerhaiti on 01/04/2017.
 */

var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var elements = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var elm = require('../../../../.././test_suite/e2e/pages/SignIn.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var variables = require('../../../../.././test_suite/e2e/specs/sprint_test/variables.js');
var emPage = require('../../../../.././test_suite/e2e/pages/explore_measures.js');


describe('Verify updated ACI data in the measure shopping cart.', function () {
    it('Sprint 19 C6810', function () {
       // I.amOnPage('https://imp0.qpp.cms.gov/?ACA=PrW9Ccxk49');
        I.click(elements.SignIn);
        I.seeInTitle(pageTitles.signInPage.SignInPage);
        I.fillField(elm.SignInPage.UserName, variables.validData.validUserName);
        I.fillField(elm.SignInPage.Password, variables.validData.validPassowrd);
        I.click(elm.SignInPage.LoginButton);
        I.seeInTitle(pageTitles.SignInPage.UserProfile);
        I.click(elements.ExploreMeasuresHeader);
        I.seeInTitle(pageTitles.exploreMeasuresPage.programPerformance);
        I.click(emPage.pageHeader.AdvancingCareInformation);
        I.moveCursorTo(emPage.advancingCareInformationPage.aciTabOne);
        I.click(emPage.advancingCareInformationPage.aciTabOne);
        I.moveCursorTo(emPage.advancingCareInformationPage.patientSpecificEducation);
        I.click(emPage.advancingCareInformationPage.patientSpecificEducation);
        I.see('The MIPS eligible clinician must use clinically relevant information from certified EHR technology to identify patient-specific educational resources and provide electronic access to those materials to at least one unique patient seen by the MIPS eligible clinician.',elm.advancingCareInformationPage.patientSpecificEducationText);
    });


});

