
    Feature ('MFA registration SMS options');

    Before((I) => {
        I.resizeWindow(1600, 1200);
        I.amOnPage('/');
    });
    Scenario('Verify First time user is able to register MFA by SMS options.'
        , function (I, header_footer,SignIn,variables, pageTitles) {

            I.see('Sign In', header_footer.pageHeader.SignIn);
            I.click(header_footer.pageHeader.SignIn);
            I.seeInTitle(pageTitles.signInPage.logInPage);
            I.see('Login',SignIn.pageHeaderNavLink.Login);
            I.fillField(SignIn.LoginPage.UserName,variables.validData.validUserName);
            I.fillField(SignIn.LoginPage.Password,variables.validData.validPassowrd);
            I.click(SignIn.LoginPage.LoginButton);
            I.seeInTitle(pageTitles.signInPage.mfaPhoneRegister);

});
