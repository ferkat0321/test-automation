//var WebdriverIO = require('webdriverio');
var webdriver = require("webdriverio");


Feature('MFA Enroll');

var screenshot;

beforeEach(function () {
    allure.description('This is a sample test.');
    allure.feature("This is feature name.");
    allure.story('This is a good story about mocha-allure-report.');

});


 beforeEach(function () {

         screenshot = allure.createStep("save", function(name) {

                 allure.createAttachment(name, function () {
                     return webdriver.screenshot();
                 });
                 [png]
             });

 });

Before((I) => {

    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});

Scenario('Verify system navigates First time user to MFA enroll page when user successfully login to QPP.', function (I, header_footer, SignIn, variables, pageTitles) {

    I.see('Sign In', header_footer.pageHeader.SignIn);
    I.click(header_footer.pageHeader.SignIn);
    I.seeInTitle(pageTitles.signInPage.logInPage);
    I.see('Login', SignIn.pageHeaderNavLink.Login);
    I.fillField(SignIn.LoginPage.UserName, variables.validData.validUserName);
    //I.see(variables.validData.validUserName,SignIn.LoginPage.UserName);
    I.fillField(SignIn.LoginPage.Password, variables.validData.validPassowrd);
    //I.see(variables.validData.validPassowrd,SignIn.LoginPage.Password);
    I.click(SignIn.LoginPage.LoginButton);
    I.seeInTitle(pageTitles.signInPage.mfaPhoneRegister);

});


// afterEach(function () {
//     if (this.currentTest.state !== "passed") {
//         return screenshot("screenshot on fail");
//     }
// });

afterEach(function (){
    var failed = false;
    var tests = this.test.parent.tests;
    for(var i = 0, limit = tests.length; !failed && i < limit; ++i)
        failed = tests[i].state === "failed";
    if (failed)
        return screenshot("screenshot on fail");
        console.log("FAILED");
});


afterEach(function () {
    return webdriver.quit;
});