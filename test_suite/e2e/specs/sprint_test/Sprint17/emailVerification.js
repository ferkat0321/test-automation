describe("Sample test case", function () {

    var changePasswordURL;

    it("should login with a registration code sent to an email", function () {
        browser.get("https://www.amazon.com/ap/signin?_encoding=UTF8&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_ya_signin");

        element(by.id("auth-fpp-link-bottom")).click();
        element(by.id("ap_email")).sendKeys("ferkat0316@hotmail.com");
        element(by.id("continue")).click();
        element(by.id("ap_zip")).sendKeys(21228);
        element(by.id("continue")).click();


        protractor.promise.controlFlow().execute(function (getLastEmail) {

            var deferred = protractor.promise.defer();
            console.log("Waiting for an email, please be patient...");

            mailListener.on("mail", function (mail) {
                deferred.fulfill(mail);
                console.log("emailParsed", mail.text);

                var text2 = mail.text;

                var String = text2.substring(text2.lastIndexOf('our secure server:') + 18, text2.lastIndexOf("If clicking the link"));
                changePasswordURL = String.replace(/\s+/, "");
                console.log("This is the final url: " + changePasswordURL);

            });

            return deferred.promise;

        }).then(function (email) {
            expect(email.subject).toEqual("Amazon.com Password Assistance");
            expect(email.headers.to).toEqual("tistaqpp@hotmail.com");


        });

    });
    it("Navigate to second url: ", function () {
        var finalUrl = changePasswordURL;
        console.log("Final url is: " + finalUrl);

        browser.get(finalUrl);
        element(by.id("ap_fpp_password")).sendKeys("19900910Fj");
        element(by.id("ap_fpp_password_check")).sendKeys("19900910Fj");
        element(by.id("continue")).click();

    })
});
