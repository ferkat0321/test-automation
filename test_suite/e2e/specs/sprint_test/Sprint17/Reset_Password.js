/**
 * Created by tislam on 12/6/2016.
 */
var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var elements=require('../../../../.././test_suite/e2e/pages/header_footer.js');
var pageTitles =require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var SignIn=require('../../../../.././test_suite/e2e/pages/SignIn.js')
var variables =require('../../../../.././test_suite/e2e/specs/sprint_test/variables.js');

describe('Reset Password', function () {
    var changePasswordURL;

    it(' Should verify user is able to reset password using Forgot password link without answering any security questions', function () {
        I.amOnPage('http://val0-webelb-677044720.us-east-1.elb.amazonaws.com/');
        I.click(elements.SignIn);
        I.seeInTitle(pageTitles.signInPage.SignInPage);
        I.click(SignIn.pageHeaderNavLink.ResetPassword);
        I.seeInTitle(pageTitles.signInPage.resetPassword);
        I.fillField(SignIn.SignInPage.UserName,SignIn.ResetPasswordPage.UserName);
        I.click(SignIn.ResetPasswordPage.ResetButton);

        protractor.promise.controlFlow().execute(function (getLastEmail) {

            var deferred = protractor.promise.defer();
            console.log("Waiting for an email...");

            mailListener.on("mail", function (mail) {
                deferred.fulfill(mail);
                console.log("emailParsed", mail.text);

                var text2 = mail.text;

                var String = text2.substring(text2.lastIndexOf('our secure server:') + 30, text2.lastIndexOf("If clicking the link"));
                changePasswordURL = String.replace(/\s+/, "");

                console.log("URL IS: " + String + "0000");
                console.log("This is the final url:---" + changePasswordURL + "---");

                //navigateToUrl();

            });

            return deferred.promise;

        }).then(function (email) {
            expect(email.subject).toEqual("Amazon.com Password Assistance");
            expect(email.headers.to).toEqual("ferkat0316@hotmail.com");


        });

        it("Navigate to reset password url: ", function () {
            var finalUrl = changePasswordURL;
            console.log("Final url is: " + finalUrl);

            browser.get(finalUrl);
            element(by.id("ap_fpp_password")).sendKeys("19900910Fj");
            browser.sleep(2000);
            element(by.id("ap_fpp_password_check")).sendKeys("19900910Fj");
            browser.sleep(2000);
            element(by.id("continue")).click();
            browser.sleep(2000);

            console.log("Title is: " + browser.getTitle);

        })

    });



});
