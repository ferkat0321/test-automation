var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var header_footer = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var em = require('../../../../.././test_suite/e2e/pages/explore_measures.js');

describe('Explore Measures page Select Measures drop down verification', function () {

    beforeEach(function () {
        I.amOnPage("");
    }),

        it('Verify Explore Measures page Specialty Measures Set drop down ', function () {
            var array = [];

            I.see('Explore Measures', header_footer.ExploreMeasuresHeader);
            I.click(header_footer.ExploreMeasuresHeader);
            I.seeInTitle(pageTitles.exploreMeasuresPage.programPerformance);
            I.see('Program Performance', em.pageHeader.ProgramPerformance);
            I.see('Quality Measures', em.pageHeader.QualityMeasures);
            I.click(em.pageHeader.QualityMeasures);
            I.moveCursorTo(em.qualityMeasuresPage.searchBox);
            I.click(em.qualityMeasuresPage.specialtyMeasureSet);

            var i;
            var m = ['14', '9', '20', '11', '14', '3', '15', '16', '19', '55',
                '14', '13', '37', '4', '25', '26', '24', '21', '21', '18',
                '8', '18', '15', '11', '17', '4', '13', '15', '12', '15'];
            for (i = 0; i < 30; i++) {
                var x = i + 1;
                var dDownList = element(by.xpath('//div[1]/div[2]/div/div/div[3]/ul/li[' + x + ']/a/input'));
                dDownList.click();

                I.click(em.qualityMeasuresPage.selectMeasuresText);
                I.see(m[i], em.qualityMeasuresPage.resultNumber);
                I.click(em.qualityMeasuresPage.specialtyMeasureSet);
                if (dDownList.isSelected()) {
                    dDownList.click();
                }
            }

        })

})

