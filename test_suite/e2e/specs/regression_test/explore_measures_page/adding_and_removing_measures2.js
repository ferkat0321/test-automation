var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var header_footer = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var em = require('../../../../.././test_suite/e2e/pages/explore_measures.js');

describe('Explore Measures page adding and removing measures verification', function () {

    beforeEach(function () {
        I.amOnPage("");
    }),

        it('Verify user can add measures on Explore Measures page ' +
            'by clicking on ADD button and Remove measures by clicking on X button.', function () {

            I.see('Explore Measures', header_footer.ExploreMeasuresHeader);
            I.click(header_footer.ExploreMeasuresHeader);
            I.seeInTitle(pageTitles.exploreMeasuresPage.programPerformance);
            I.see('Quality Measures', em.pageHeader.QualityMeasures);
            I.click(em.pageHeader.QualityMeasures);
            I.moveCursorTo(em.qualityMeasuresPage.searchBox);

            var i;
            var maxMeasuresNum = 20;

            for (i = 1; i < maxMeasuresNum; i++) {
                var addRemoveButton = element(by.xpath('//section/div/div[5]/div[2]/div[' + i + ']/div/div[1]/p/button'));
                addRemoveButton.click();
                I.see(i.toString(), em.qualityMeasuresPage.measuresCount);
                //console.log('Added mesasure number: '+i.toString());
            }

            var x;
            var a;
            var b = maxMeasuresNum - 2;
            for (x = b; x > 0; x--) {

                a = x + 2;
                var xButton = element(by.xpath('//section/div/div[5]/div[1]/div/div[2]/div[' + a + ']/a'));
                xButton.click();
                I.see(x.toString(), em.qualityMeasuresPage.measuresCount);
                //console.log('Removed mesasure number: '+x.toString());

            }

        })
})




