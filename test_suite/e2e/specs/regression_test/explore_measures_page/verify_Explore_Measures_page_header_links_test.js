/**
 * Created by fuerhaiti on 11/18/16.
 */


Feature('Explore Measures page verification');

Before((I) => {
    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});
Scenario('VerifyExplore Measures page header links', function *(I, header_footer,explore_measures,pageTitles) {

    I.see('Explore Measures', header_footer.pageHeader.ExploreMeasuresHeader);
    I.click(header_footer.pageHeader.ExploreMeasuresHeader);
    I.seeInTitle(pageTitles.exploreMeasuresPage.programPerformance);
    I.see('Program Performance',explore_measures.pageHeaderNavLink.ProgramPerformance);
    I.see('Quality Measures',explore_measures.pageHeaderNavLink.QualityMeasures);
    I.see('Advancing Care Information',explore_measures.pageHeaderNavLink.AdvancingCareInformation);
    I.see('Improvement Activities',explore_measures.pageHeaderNavLink.ImprovementActivities);

});
