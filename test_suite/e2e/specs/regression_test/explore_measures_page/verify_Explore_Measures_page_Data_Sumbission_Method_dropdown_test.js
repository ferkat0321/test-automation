var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var header_footer = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var em = require('../../../../.././test_suite/e2e/pages/explore_measures.js');

describe('Explore Measures page Select Measures drop down verification', function () {
    beforeEach(function () {
        I.amOnPage("");
    }),


        it('Verify Explore Measures page Data Submission Method drop down ', function () {
            I.turnSyncOff();
            I.see('Explore Measures', header_footer.ExploreMeasuresHeader);
            I.click(header_footer.ExploreMeasuresHeader);
            I.seeInTitle(pageTitles.exploreMeasuresPage.programPerformance);
            I.see('Program Performance', em.pageHeader.ProgramPerformance);
            I.see('Quality Measures', em.pageHeader.QualityMeasures);
            I.click(em.pageHeader.QualityMeasures);
            I.moveCursorTo(em.qualityMeasuresPage.searchBox);
            I.click(em.qualityMeasuresPage.dataSubmissionMethod);
            var i;
            var n = ['1', '74', '1', '15', '53', '243']
            for (i = 0; i < 6; i++) {
                I.selectDropDownElements(em.qualityMeasuresPage.dataSubmissionDropDown, i);
                I.click(em.qualityMeasuresPage.resultNumber);
                I.see(n[i], em.qualityMeasuresPage.resultNumber);
                I.click(em.qualityMeasuresPage.dataSubmissionMethod);
                I.unselectDropDown(em.qualityMeasuresPage.dataSubmissionDropDown, i);
            }

        })
})

