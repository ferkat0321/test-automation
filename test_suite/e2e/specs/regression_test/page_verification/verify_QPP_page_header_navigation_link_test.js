/**
 * Created by fuerhaiti on 11/21/16.
 */

var assert = require('assert');

Feature('Verify QPP page header navigation links.');

Before((I) => {
    I.resizeWindow(1290, 1200);
    I.amOnPage('/');
});

Scenario('Verify QPP page header navigation links', function *(I, header_footer,pageTitles) {
    //This step is navigating to main page and verifies "Learn About the Program" link exists and clicks on it
    //and verifies user is navigated to "Learn About the Program" page by verifying the page title
    I.see('Learn About the Program', header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.click(header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);

    //This step is clicking on "Explore Measures" header link to navigate to "Explore Measures" page
    //and verifies the page title to make sure the user is on correct page.
    I.see('Explore Measures', header_footer.pageHeader.ExploreMeasuresHeader);
    I.click(header_footer.pageHeader.ExploreMeasuresHeader);
    I.seeInTitle(pageTitles.exploreMeasuresPage.qualityMeasures);

    //This step is clicking on "Education & Tools" header link to navigate to "Education & Tools" page
    //and verifies the page title to make sure the user is on correct page.
    I.see('Education & Tools', header_footer.pageHeader.EducationToolsHeader);
    I.click(header_footer.pageHeader.EducationToolsHeader);
    I.seeInTitle(pageTitles.educationToolsPage.eTools);

    //This step is clicking on "Sign In" header link to navigate to "Sign In" page
    //and verifies the page title to make sure the user is on correct page.
    I.see('Sign In', header_footer.pageHeader.SignIn);
    I.click(header_footer.pageHeader.SignIn);
    I.seeInTitle('Login - Quality Payment Program');

});

