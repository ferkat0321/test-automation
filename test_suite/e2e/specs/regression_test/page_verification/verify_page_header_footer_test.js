var assert = require('assert');

Feature('Page Header & Footer Verification');

Before((I) => {

    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});

Scenario('Verify QPP page headers.', function*(I, header_footer) {
    I.see('Quality Payment Program', header_footer.pageHeader.QualityPaymentProgramHeaderTitle);
    I.see('Learn About the Program', header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.see('Explore Measures', header_footer.pageHeader.ExploreMeasuresHeader);
    I.see('Education & Tools', header_footer.pageHeader.EducationToolsHeader);
    I.see('Sign In', header_footer.pageHeader.SignIn);
});


Scenario('Verify QPP page footers.', function*(I, header_footer) {
    I.moveCursorTo(header_footer.pageFooter.LearnAbouttheProgramFooter);
    I.see('Quality Payment Program', header_footer.pageFooter.QualityPaymentProgram);
    I.see('LEARN MORE ABOUT THE QUALITY PAYMENT PROGRAM', header_footer.pageFooter.LearnMoreQPPTitle);
    I.see('Quality Payment Program', header_footer.pageFooter.QualityPaymentProgram);
    I.see('Learn About the Program', header_footer.pageFooter.LearnAbouttheProgramFooter);
    I.see('Explore Measures', header_footer.pageFooter.ExploreMeasuresFooter);
    I.see('Education & Tools', header_footer.pageFooter.EducationToolsFooter);
    I.see('PRIVACY AND ACCESSIBILITY', header_footer.pageFooter.PolicyCenterTitle);
    I.see('CMS Privacy Notice', header_footer.pageFooter.CMSPrivacyNotice);
    I.see('Accessibility', header_footer.pageFooter.Accessibility);
    I.see('CONTACT THE QUALITY PAYMENT PROGRAM SERVICE CENTER', header_footer.pageFooter.ServiceCenterTitle);
    I.see('Send Us Your Questions', header_footer.pageFooter.EmailContact);
    I.see('Subscribe to Email Updates', header_footer.pageFooter.EmailSubscribe);
    I.seeElement(header_footer.pageFooter.CMSImg);
});







