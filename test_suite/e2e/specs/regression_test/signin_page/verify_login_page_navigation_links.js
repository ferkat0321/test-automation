<<<<<<< HEAD
var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var header_footer = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var SignIn =require('../../../../.././test_suite/e2e/pages/SignIn.js');

describe('Verify login page header elements.', function () {

    beforeEach(function () {
        I.amOnPage("/secure/login");
    }),

    it('This scenario verifies Log in page header elements.', function () {
        I.see('Sign In', header_footer.SignIn);
        I.click(header_footer.SignIn);
        I.seeInTitle(pageTitles.signInPage.SignInPage);


    })
=======
/**
 * Created by fuerhaiti on 11/18/16.
 */


Feature('Verify login page header elements.');

Before((I) => {
    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});
Scenario('This scenario verifies Log in page header elements.', function *(I, header_footer,SignIn,pageTitles) {

    I.see('Sign In', header_footer.pageHeader.SignIn);
    I.click(header_footer.pageHeader.SignIn);
    I.seeInTitle(pageTitles.signInPage.logInPage);
    I.see('Login',SignIn.pageHeaderNavLink.Login);
    I.see('User Details',SignIn.pageHeaderNavLink.UserDetails);
    I.see('Reset Password',SignIn.pageHeaderNavLink.ResetPassword);
    I.see('Recover User ID',SignIn.pageHeaderNavLink.RecoverUserId);
    I.see('Unlock Account',SignIn.pageHeaderNavLink.UnlockAccount);

>>>>>>> 63db5967108cab2f661bd69adeaf82935ab73588

});
