var I = require('../../../helpers/methodLibrary.js');
var header_footer=require('../../../pages/header_footer.js');
var pageTitles =require('../../../pages/pageTitles.js');
var SignIn= require('../../../pages/SignIn.js');
var variables= require('../../sprint_test/variables.js');


describe('Verify login page functions.', function () {

    beforeEach(function () {
        I.amOnPage("/secure/login");
    }),


    it('This scenario verifies error message when user clicks on log in without entering user name and password', function () {

        I.see('Sign In', header_footer.SignIn);
        I.click(header_footer.SignIn);
        I.seeInTitle(pageTitles.signInPage.userDetails);
        I.see('Login',SignIn.pageHeaderNavLink.Login);
        I.click(SignIn.SignInPage.LoginButton);
    }),

        it('This scenario verifies error message when user tries to log in with invalid user name and invalid password.', function () {

            I.see('Sign In', header_footer.SignIn);
            I.click(header_footer.SignIn);
            I.seeInTitle(pageTitles.signInPage.SignInPage);
            I.see('Login',SignIn.pageHeaderNavLink.Login);
            I.fillField(SignIn.SignInPage.UserName,variables.invalidData.invalidUserName);
            I.fillField(SignIn.SignInPage.Password,variables.invalidData.invalidPassowrd);
            I.click(SignIn.SignInPage.LoginButton);
            I.waitForElement(SignIn.SignInPage.ErrorMessage);
            I.see('Error in Get User',SignIn.SignInPage.ErrorMessage);
        }),

        it('This scenario verifies error message when user tries to log in with invalid user name and valid password.', function () {

            I.see('Sign In', header_footer.SignIn);
            I.click(header_footer.SignIn);
            I.seeInTitle('Login - Quality Payment Program');
            I.see('Login',SignIn.pageHeaderNavLink.Login);
            I.fillField(SignIn.SignInPage.UserName,variables.invalidData.invalidUserName);
            I.fillField(SignIn.SignInPage.Password,variables.validData.validPassowrd);
            I.click(SignIn.SignInPage.LoginButton);
            I.waitForElement(SignIn.SignInPage.ErrorMessage);
            I.see('Error in Get User',SignIn.SignInPage.ErrorMessage);
        }),

        it('This scenario verifies error message when user tries to log in with valid user name and invalid password.', function () {

            I.see('Sign In', header_footer.SignIn);
            I.click(header_footer.SignIn);
            I.seeInTitle('Login - Quality Payment Program');
            I.see('Login',SignIn.pageHeaderNavLink.Login);
            I.fillField(SignIn.SignInPage.UserName,variables.validData.validUserName);
            I.fillField(SignIn.SignInPage.Password,variables.invalidData.invalidPassowrd);
            I.click(SignIn.SignInPage.LoginButton);
            I.waitForElement(SignIn.SignInPage.ErrorMessage);
            I.see('User is not Staged',SignIn.SignInPage.ErrorMessage);
        })


})
