/**
 * Created by fuerhaiti on 12/23/16.
 */

//12345Ngc  Imp0

var I = require('../../../.././test_suite/e2e/helpers/methodLibrary.js');
var elements = require('../../../.././test_suite/e2e/pages/header_footer.js');
var elm = require('../../../.././test_suite/e2e/pages/SignIn.js');
var pageTitles = require('../../../.././test_suite/e2e/pages/pageTitles.js');
var variables = require('../../../.././test_suite/e2e/specs/sprint_test/variables.js');


describe('Change Password after login.', function () {

    beforeEach(function () {
        I.amOnPage("/secure/login");
    }),

    it('As a QPP user, I need to reset my password using Forgot password link without answering any security question', function () {
        I.seeInTitle(pageTitles.signInPage.SignInPage);
        I.fillField(elm.SignInPage.UserName, variables.validData.validUserName);
        I.fillField(elm.SignInPage.Password, variables.validData.validPassowrd);
        I.click(elm.SignInPage.LoginButton);
        I.seeInTitle(pageTitles.SignInPage.UserProfile);
        I.click(elm.SignInPage.ChangePasswordPage.changePassowrd);
        I.fillField(elm.ChangePasswordPage.oldPassword, variables.validData.validPassowrd);
        I.fillField(elm.ChangePasswordPage.newPasswordBox, variables.validNewData.newPassword);
        I.fillField(elm.ChangePasswordPage.confirmPassword, variables.validNewData.newPassword);
        I.click(elm.ChangePasswordPage.changePasswordButton);


        //This test only missing getting email and verify the message.
        //In order to get the email we need MFA be turned off.

        protractor.promise.controlFlow().execute(function (getLastEmail) {

            var deferred = protractor.promise.defer();
            console.log("Waiting for an email, please be patient...");

            mailListener.on("mail", function (mail) {
                deferred.fulfill(mail);
                var json = JSON.stringify(mail);
                var obj = json;

                var String = JSON.parse(json).html.replace(/\s/g, '');
                console.log("@@@"+String+"@@@");

                changePasswordUrl = String.substring(169, 264);

                console.log("***" + changePasswordUrl + "***");

            });

            mailListener.on("error", function (err) {
                console.log(err);
            });

            return deferred.promise;

        }).then(function (email) {
            expect(email.subject).toEqual('Your password has been changed');
            expect(email.headers.to).toEqual('<tistaqpp@hotmail.com>');

        });

    });



});

