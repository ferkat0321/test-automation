/**
 * Created by fuerhaiti on 12/23/16.
 * Updated by TJ on 01/05/2016
 */

var I = require('../../../.././test_suite/e2e/helpers/methodLibrary.js');
var elements = require('../../../.././test_suite/e2e/pages/header_footer.js');
var elm = require('../../../.././test_suite/e2e/pages/SignIn.js');
var pageTitles = require('../../../.././test_suite/e2e/pages/pageTitles.js');
var variables = require('../../../.././test_suite/e2e/specs/sprint_test/variables.js');
var invalidPasswords = variables.invalidPasswords;


describe('Change Password after login. Test Password Rules.', function () {

    beforeEach(function () {
        I.amOnPage("/secure/login");
    }),

    it('As a QPP user, I need to reset my password using Forgot password link without answering any security question', function () {
        I.seeInTitle(pageTitles.signInPage.SignInPage);
        I.fillField(elm.SignInPage.UserName, variables.validData.validUserName);
        I.fillField(elm.SignInPage.Password, variables.validData.validPassowrd);
        I.click(elm.SignInPage.LoginButton);
        I.seeInTitle(pageTitles.SignInPage.UserProfile);
        I.click(elm.SignInPage.ChangePasswordPage.changePassowrd);



        for(i =  0; i < invalidPasswords.length(); i++){
            //Checking syntax rules
            I.fillField(elm.ChangePasswordPage.oldPassword, variables.validData.validPassowrd);
            I.fillField(elm.ChangePasswordPage.newPasswordBox, variables.invalidData[i]);
            I.fillField(elm.ChangePasswordPage.confirmPassword, variables.invalidData[i]);
            I.click(elm.ChangePasswordPage.changePasswordButton);//TODO - Verify Message
        }

        //Looping through valid passwords
        for(var i = 0; i < 6; i++){
            I.fillField(elm.ChangePasswordPage.oldPassword, variables.validData.validPassowrd);
            I.fillField(elm.ChangePasswordPage.newPasswordBox, makePassword());
            I.fillField(elm.ChangePasswordPage.confirmPassword, makePassword());
            I.click(elm.ChangePasswordPage.changePasswordButton);
            //TODO - Verify Message
        }
        I.fillField(elm.ChangePasswordPage.oldPassword, variables.validData.validPassowrd);
        I.fillField(elm.ChangePasswordPage.newPasswordBox, variables.validData.validPassowrd);
        I.fillField(elm.ChangePasswordPage.confirmPassword, variables.validData.validPassowrd);
        I.click(elm.ChangePasswordPage.changePasswordButton);


    });
    function makePassword(){
        var password = '';
        var upLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var downLetters = 'abcdefghijklmnopqrstuvwxyz';
        var numbers = '1234567890';

        for(var i = 0; i < 9; i++){
            if(i = 3){
                password += upLetters.charAt(Math.floor(Math.random() * possible.length));
            }else if(i = 4){
                password += numbers.charAt(Math.floor(Math.random() * possible.length));
            }else{
                password += downLetters.charAt(Math.floor(Math.random() * possible.length));
            }
        }

        return password;
    }


});

