var I = require('../../../../.././test_suite/e2e/helpers/methodLibrary.js');
var header_footer = require('../../../../.././test_suite/e2e/pages/header_footer.js');
var pageTitles = require('../../../../.././test_suite/e2e/pages/pageTitles.js');
var learn_about_the_program = require('../../../../.././test_suite/e2e/pages/learn_about_the_program.js');
var homePage = require('../../../../.././test_suite/e2e/pages/homepage.js');


describe('Learn About the Program page verification', function () {

        beforeEach(function () {
            I.amOnPage("");
        }),

        it('Verify Learn About the Program', function () {

            I.see('Learn About the Program', header_footer.LearnAbouttheProgramHeader);
            I.click(header_footer.LearnAbouttheProgramHeader);
            I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);

        }),

        it('Verify Learn About the Program Page Header Links.', function () {

            I.see('Learn About the Program', header_footer.LearnAbouttheProgramHeader);
            I.click(header_footer.LearnAbouttheProgramHeader);
            I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);
            I.see('How do I Participate in Alternative Payment Models?', learn_about_the_program.HowDoIParticipateInAlternativePaymentModules);
            I.see('How Do I Participate in the Program?', learn_about_the_program.HowDoIParticipateInProgram);
            I.see('What Can I do Now?', learn_about_the_program.WhatCanIDoNow);

        })

    it('Verify Next: Participate in An Advanced APM  Button', function () {

        //This step navigates to QPP main page and navigate to Learn about the program page
        // then scroll down to "Next: in An Advanced APM  Button" and click it
        I.see('Learn About the Program', header_footer.LearnAbouttheProgramHeader);
        I.click(header_footer.LearnAbouttheProgramHeader);
        I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);
        I.moveCursorTo(learn_about_the_program.NextParticipateInProgramButton);
        I.click(learn_about_the_program.NextParticipateInProgramButton);
        I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdPaymentModel);

    }),

        it('Verify Next: What Can I Do Now  Button', function () {

            //This step navigates to QPP main page and navigate to Learn about the program page
            // then scroll down to "Next: What Can I Do Now Button" and click it
            I.see('Learn About the Program', header_footer.LearnAbouttheProgramHeader);
            I.click(header_footer.LearnAbouttheProgramHeader);
            I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);
            I.click(learn_about_the_program.HowDoIParticipateInAlternativePaymentModules);
            I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdPaymentModel);
            I.moveCursorTo(learn_about_the_program.NextParticipateInProgramButton);
            I.click(learn_about_the_program.NextParticipateInProgramButton);
            I.seeInTitle(pageTitles.learnAboutTheProgramPage.whatCanIdo);

        }),

        it('Verify Next: Learn About the Program Button', function () {
            //This step navigates to QPP main page and scroll down to
            //Next: Learn About the Program Button and click on it
            I.moveCursorTo(homePage.NextLearnAbouttheProgramButton);
            I.click(homePage.NextLearnAbouttheProgramButton);
            I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);

        })


});


