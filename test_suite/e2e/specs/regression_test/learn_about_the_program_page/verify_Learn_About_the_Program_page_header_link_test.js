var assert = require('assert');


Feature('Learn About the Program Page Header Link Verification');

Before((I) => {

    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});

Scenario('Verify Learn About the Program Page Header Links.', function*(I, header_footer,learn_about_the_program,pageTitles) {
    I.see('Learn About the Program', header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.click(header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);
    I.see('How Do I Participate in Alternative Payment Models?',learn_about_the_program.pageHeaderNavLink.HowDoIParticipateInAlternativePaymentModules);
    I.see('How Do I Participate in the Program?',learn_about_the_program.pageHeaderNavLink.HowDoIParticipateInProgram);
    I.see('What Can I Do Now?',learn_about_the_program.pageHeaderNavLink.WhatCanIDoNow);
});









