/**
 * Created by fuerhaiti on 11/21/16.
 */

Feature('Verify Next: What Can I Do Now  Button');

Before((I) => {
    I.resizeWindow(1290, 1200);
    I.amOnPage('/');
});

Scenario('Verify Next: What Can I Do Now  Button', function *(I,learn_about_the_program,header_footer,pageTitles) {

    //This step navigates to QPP main page and navigate to Learn about the program page
    // then scroll down to "Next: What Can I Do Now Button" and click it
    I.see('Learn About the Program', header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.click(header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);
    I.click(learn_about_the_program.pageHeaderNavLink.HowDoIParticipateInAlternativePaymentModules);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdPaymentModel);
    I.moveCursorTo(learn_about_the_program.pageHeaderNavLink.NextWhatCanIDoNowButton);
    I.click(learn_about_the_program.pageHeaderNavLink.NextWhatCanIDoNowButton);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.whatCanIdo);

});




