/**
 * Created by fuerhaiti on 11/21/16.
 */

Feature('Verify Next: in An Advanced APM  Button');

Before((I) => {
    I.resizeWindow(1290, 1200);
    I.amOnPage('/');
});

Scenario('Verify Next: in An Advanced APM  Button', function *(I,header_footer,pageTitles) {

    //This step navigates to QPP main page and navigate to Learn about the program page
    // then scroll down to "Next: in An Advanced APM  Button" and click it
    I.see('Learn About the Program', header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.click(header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);
    I.moveCursorTo('.btn.btn-success');
    I.click('.btn.btn-success');
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdPaymentModel);

});




