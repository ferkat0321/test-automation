/**
 * Created by fuerhaiti on 11/21/16.
 */

Feature('Verify Next: Learn About the Program Button');

Before((I) => {
    I.resizeWindow(1290, 1200);
    I.amOnPage('/');
});

Scenario('Verify Next: Learn About the Program Button', function *(I,pageTitles) {

    //This step navigates to QPP main page and scroll down to
    //Next: Learn About the Program Button and click on it
    I.moveCursorTo('.btn.btn-success');
    I.click('.btn.btn-success');
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);

});




