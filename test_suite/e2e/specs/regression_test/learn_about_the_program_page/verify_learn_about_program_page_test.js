/**
 * Created by fuerhaiti on 11/18/16.
 */


Feature('Learn About the Program page verification');

Before((I) => {
    I.resizeWindow(1600, 1200);
    I.amOnPage('/');
});
Scenario('Verify Learn About the Program', function *(I, header_footer,pageTitles) {

    I.see('Learn About the Program', header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.click(header_footer.pageHeader.LearnAbouttheProgramHeader);
    I.seeInTitle(pageTitles.learnAboutTheProgramPage.hdProgram);

});
