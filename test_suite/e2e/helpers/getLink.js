/**
 * Created by fuerhaiti on 12/9/16.
 */
var extractUrl= function (done) {

    //startingPoint    our secure server:
    //startingIndex    30
    //endingPoint      If clicking the link

    var deferred = protractor.promise.defer();
    console.log("Waiting for an email...");

    mailListener.on("mail", function (mail) {
        deferred.fulfill(mail);
        console.log("emailParsed", mail.text);

        var text2 = mail.text;

        var String = text2.substring(text2.lastIndexOf('our secure server:') + 18, text2.lastIndexOf("If clicking the link"));
        var changePasswordURL = String.replace(/\s+/, "");
        console.log("This is the change password url: " + changePasswordURL);

        return changePasswordURL;

    }).finally(done);

    return deferred.promise;

}