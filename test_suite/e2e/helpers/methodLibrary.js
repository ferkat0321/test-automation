'use strict';

var origFn = browser.driver.controlFlow().execute;
var pg = require('pg');
var conf=require('../../../local.conf').config;
var userId;


module.exports = {

    amOnPage: function () {
        browser.manage().window().maximize();
        browser.get(conf.baseUrl);
    },

    click: function (elm) {
        elm.click();
    },

    fillField: function (elm, text) {
        elm.type(text);
    },

    type: function (elm, text) {
        elm.sendKeys(text);
    },

    grabTitle: function () {
        let pageTitle = browser.getTitle();
        console.log(pageTitle);
        return pageTitle;
    },

    seeInTitle: function (title) {
        expect(browser.getTitle()).toEqual(title);
    },

    seeInUrl: function (txt) {
        expect(browser.getLocationAbsUrl())
            .toBe(txt);
    },

    moveCursorTo: function (elm) {
        browser.actions().mouseMove(elm).perform();
    },

    waitForElement: function () {
        browser.waitForAngular();
    },

    see: function (text, elm) {
        expect(elm.getText()).toEqual(text);
    },

    seeElement: function (elm) {
        expect(elm.isDisplayed);
    },

    grabTextFrom: function (elm) {
        let text = elm.getText();
        text.then(function (finalText) {
            return finalText;
        })
    },


    setSpeed: function (time) {
        browser.driver.controlFlow().execute = function () {
            var args = arguments;

            // queue 100ms wait
            origFn.call(browser.driver.controlFlow(), function () {
                return protractor.promise.delayed(time);
            });

            return origFn.apply(browser.driver.controlFlow(), args);
        };
    },

    turnSyncOff: function () {
        browser.ignoreSynchronization = true;
    },

    turnSyncOn: function () {
        browser.ignoreSynchronization = false;
    },

    getDropDownListLength: function (elm) {

        //var dDownlength= elm.length;
        elm.then(function (items, done) {
            var dDownlength = items.length;
            console.log("Length of the drop down list: " + dDownlength);
            done;
            return dDownlength;
        });
    },

    selectDropDownElements: function (elm, num) {
        elm.then(function (items) {
            items[num].click();
        });

    },

    unselectDropDown: function (elm, num) {
        elm.then(function (items) {
            if (items[num].isSelected()) {
                items[num].click();
            }
        });
    },

    connectToDb: function (dbUserName, dbPassword, dbHost, dbName) {
        var connectionString = "postgres://" + dbUserName + ":" + dbPassword + "@" + dbHost + "/" + dbName;
        var pgClient = new pg.Client(connectionString);
        pgClient.connect();

        return pgClient;

    },

    extractUserId: function () {
        protractor.promise.controlFlow().execute(function (getLastEmail) {

            var deferred = protractor.promise.defer();
            console.log("Waiting for an email, please be patient...");

            mailListener.on("mail", function (mail) {
                deferred.fulfill(mail);
                var json = JSON.stringify(mail);
                var obj = json;

                var String = JSON.parse(json).html.replace(/\s/g, '');

                userId = String.substring(62, 71);
                console.log("====" + userId);

            });

            mailListener.on("error", function (err) {
                console.log(err);
            });

            // return deferred.promise;


        })
        // .then(function (email) {
        //     expect(email.subject).toEqual('Your I & A user ID');
        //     expect(email.headers.to).toEqual('<tistaqpp@hotmail.com>');
        //
        // });


    }

};


