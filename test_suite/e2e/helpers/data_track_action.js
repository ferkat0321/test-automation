var dataTrackAction = function () {
    by.addLocator('dataTrackAction', function (toState, parentelement) {
        var using = parentelement || document;
        var prefixes = ['data-track-action'];
        for (var p = 0; p < prefixes.length; ++p) {
            var selector = '*[' + prefixes[p] + '="' + toState + '"]';
            var inputs = using.querySelectorAll(selector);
            if (inputs.length) {
                return inputs;
            }
        }
    });
}
module.exports = new dataTrackAction();
