<<<<<<< HEAD
'use strict';

module.exports = {
    SignInPage: {
        SignIn: element(by.xpath('//section/div/div/div/h1')),
        text: element(by.xpath('//section/div/div/div/p')),
        UserName: element(by.id('username')),
        Password: element(by.id('password')),
        LoginButton: element(by.xpath('//section/div/div[2]/div/form/div[4]/div/button')),
        ForgotUserId: element(by.xpath('//section/div/div[3]/div/p[1]/a[1]')),


        // EmailTextBox: element(by.xpath('//section/div/div/div/div/div/form/input[2]')),
        // SendResetPasswordButton: element(by.xpath('//section/div/div/div/div/div/form/div/div/p[1]/button'))
    },

    ChangePasswordPage:{
        changePassowrd: element(by.xpath('//html/body/main/nav/div/ul/li[3]/a')),
        oldPassword: element(by.id('old-password')),
        newPassword: element(by.id('new-password')),
        confirmPassword: element(by.id('confirm-password')),
        changePasswordButton: element(by.xpath('//section/div/div[2]/div/form/button'))

    },


    ChangeYourPasswordLinkPage: {
        newPasswordBox: element(by.id('new-password')),
        confirmPasswordBox: element(by.id('password')),
        changePasswordButton: element(by.xpath('//section/div/div[2]/div/form/div[5]/div/button')),
        showMyTyping: element(by.xpath('//section/div/div/div/div/div/form/p/a'))

    },

    ForgotUserIdPage: {
        h1: element(by.xpath('//section/div/div/div/h1')),
        text: element(by.xpath('//section/div/div/div/p[3]')),
        ForgotPassword: element(by.xpath('//section/div/div[3]/div/p[1]/a[2]')),
        userId: element(by.id('user-id')),
        sendPasswordButton: element(by.xpath('//section/div/div[2]/div/form/button'))
    }





    // UserDetailsPage: {
    //     userDetailsUserName: 'html/body/ng-view/section/div/div/h2[1]',
    //     userDetailsFirstName: 'html/body/ng-view/section/div/div/h2[2]',
    //     userDetailsLastName: 'html/body/ng-view/section/div/div/h2[3]'
    // },
    // ResetPasswordPage: {
    //     UserName: '.ng-pristine.ng-untouched.ng-valid.ng-empty',
    //     ResetButton: '.row.animate-switch.ng-scope>button'
    // },
    // RecoverUserIdPage: {
    //     FirstName: 'html/body/ng-view/section/form/div/div[2]/input[1]',
    //     LastName: 'html/body/ng-view/section/form/div/div[2]/input[2]',
    //     Email: 'html/body/ng-view/section/form/div/div[2]/input[3]',
    //     RecoverUserIdButton: '.row>button'
    // },
    // changePasswordPage: {
    //     oldPassword: 'html/body/ng-view/section/div/div[2]/div/input[1]',
    //     newPassword: 'html/body/ng-view/section/div/div[2]/div/input[2]',
    //     confirmPassword: 'html/body/ng-view/section/div/div[2]/div/input[3]',
    //     changeButton: 'html/body/ng-view/section/div/div[2]/div/button',
    //     successMessage: 'html/body/ng-view/section/div/div[1]/h2'
    // },
    // UnlockAccountPage: {
    //     UserName: '.ng-pristine.ng-untouched.ng-empty.ng-invalid.ng-invalid-required',
    //     EmailButton: 'html/body/ng-view/section/div/div[2]/div/div[1]/button',
    //     SMSButton: 'html/body/ng-view/section/div/div[2]/div/div[2]/button'
    // },
    // registerMfaPage: {
    //     sendCodeCall: '#.col-sm-2>button',
    //     callTxtBox: '#.ng-pristine.ng-valid.ng-empty.ng-touched',
    //     validateCodeCall: '#.col-sm-5>button',
    //     sendCodeSms: '#.col-sm-2>button',
    //     smsTxtBox: '#.ng-valid.ng-touched.ng-not-empty.ng-dirty.ng-valid-parse',
    //     validateCodeSms: '#.col-sm-5>button',
    //     systemMessage: '#.text-danger.ng-binding'
    // },
    // updateMfa: {
    //     sms: {
    //         mfaTypeSms: '#.col-sm-2.ng-binding',
    //         phoneNumberBoxSms: '#.ng-pristine.ng-untouched.ng-valid.ng-not-empty.ng-valid-maxlength',
    //         updateMfaSms: '#.ng-scope>button',
    //         validateSmsBox: '#.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-maxlength',
    //         validateSmsCode: '#.col-sm-3>button',
    //         removeSmsButton: '#.ng-scope>button'
    //     },
    //     call: {
    //         mfaTypeCall: '#.col-sm-2.ng-binding',
    //         phoneNumberBoxCall: '#.ng-pristine.ng-untouched.ng-valid.ng-not-empty.ng-valid-maxlength',
    //         updateMfaCall: '#.ng-scope>button',
    //         validateCallBox: '#.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-maxlength',
    //         validateCallCode: '#.col-sm-3>button',
    //         removeCallButton: '#.ng-scope>button'
    //     }
    // },
    // pageHeaderNavLink: {
    //     Login: element(by.id('idmHeaderMobile')),
    //     UserDetails: '#userDetailsHeaderMobile',
    //     ResetPassword: '#resetPasswordHeaderMobile',
    //     RecoverUserId: '#recoverUserIdHeaderMobile',
    //     UnlockAccount: '#unlockAccountHeaderMobile'
    // },
=======
/**
 * Created by fuerhaiti on 11/22/16.
 */

module.exports = {

    pageHeaderNavLink: {
        Login: '#loginHeaderMobile',
        UserDetails: '#userDetailsHeaderMobile',
        ResetPassword: '#resetPasswordHeaderMobile',
        RecoverUserId: '#recoverUserIdHeaderMobile',
        UnlockAccount: '#unlockAccountHeaderMobile'
    },
    LoginPage: {
        UserName: '.ng-pristine.ng-untouched.ng-empty.ng-invalid.ng-invalid-required',
        Password: '.ng-pristine.ng-untouched.ng-empty.ng-invalid.ng-invalid-required',
        LoginButton: '.row>button',
        ErrorMessage: '.text-danger.ng-binding',
        logOutButton:'#logoutHeaderMobile'
    },
    UserDetailsPage: {
        userDetailsUserName:'html/body/ng-view/section/div/div/h2[1]',
        userDetailsFirstName:'html/body/ng-view/section/div/div/h2[2]',
        userDetailsLastName:'html/body/ng-view/section/div/div/h2[3]'
    },
    ResetPasswordPage: {
        UserName: '.ng-pristine.ng-untouched.ng-valid.ng-empty',
        ResetButton: '.row.animate-switch.ng-scope>button'
    },
    RecoverUserIdPage: {
        FirstName: 'html/body/ng-view/section/form/div/div[2]/input[1]',
        LastName: 'html/body/ng-view/section/form/div/div[2]/input[2]',
        Email: 'html/body/ng-view/section/form/div/div[2]/input[3]',
        RecoverUserIdButton: '.row>button'
    },
    changePasswordPage:{
        oldPassword:'html/body/ng-view/section/div/div[2]/div/input[1]',
        newPassword:'html/body/ng-view/section/div/div[2]/div/input[2]',
        confirmPassword:'html/body/ng-view/section/div/div[2]/div/input[3]',
        changeButton:'html/body/ng-view/section/div/div[2]/div/button',
        successMessage:'html/body/ng-view/section/div/div[1]/h2'
    },
    UnlockAccountPage: {
        UserName: '.ng-pristine.ng-untouched.ng-empty.ng-invalid.ng-invalid-required',
        EmailButton: 'html/body/ng-view/section/div/div[2]/div/div[1]/button',
        SMSButton: 'html/body/ng-view/section/div/div[2]/div/div[2]/button'

    },
    registerMfaPage: {
        sendCodeCall: '#.col-sm-2>button',
        callTxtBox: '#.ng-pristine.ng-valid.ng-empty.ng-touched',
        validateCodeCall: '#.col-sm-5>button',
        sendCodeSms: '#.col-sm-2>button',
        smsTxtBox: '#.ng-valid.ng-touched.ng-not-empty.ng-dirty.ng-valid-parse',
        validateCodeSms: '#.col-sm-5>button',
        systemMessage: '#.text-danger.ng-binding'

    },
    updateMfa: {
        sms: {
            mfaTypeSms: '#.col-sm-2.ng-binding',
            phoneNumberBoxSms: '#.ng-pristine.ng-untouched.ng-valid.ng-not-empty.ng-valid-maxlength',
            updateMfaSms: '#.ng-scope>button',
            validateSmsBox: '#.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-maxlength',
            validateSmsCode: '#.col-sm-3>button',
            removeSmsButton: '#.ng-scope>button'
        },

        call: {
            mfaTypeCall: '#.col-sm-2.ng-binding',
            phoneNumberBoxCall: '#.ng-pristine.ng-untouched.ng-valid.ng-not-empty.ng-valid-maxlength',
            updateMfaCall: '#.ng-scope>button',
            validateCallBox: '#.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-maxlength',
            validateCallCode: '#.col-sm-3>button',
            removeCallButton: '#.ng-scope>button'

        }

    }


>>>>>>> 63db5967108cab2f661bd69adeaf82935ab73588
};