'use strict';
var ngclick = require('../../.././test_suite/e2e/helpers/ngClick.js');
var trackAction = require('../../.././test_suite/e2e/helpers/data_track_action.js');

module.exports = {

<<<<<<< HEAD
=======
        pageHeader: {
            QualityPaymentProgramHeaderTitle: '#lnkQPPHome',
            LearnAbouttheProgramHeader: '#learnHeaderMobile',
            ExploreMeasuresHeader: '#measuresHeaderMobile',
            EducationToolsHeader: '#toolseduHeaderMobile',
            SignIn: '#idmHeaderMobile',
        },
        pageFooter:{
            QualityPaymentProgramFooterTitle:'#h-footerTitle',
>>>>>>> 63db5967108cab2f661bd69adeaf82935ab73588

    QualityPaymentProgramHeaderTitle: element(by.xpath('//nav/div/div/a')),
    LearnAbouttheProgramHeader: element(by.xpath('//nav/div/nav/ul/li[1]/a')),
    ExploreMeasuresHeader: element(by.xpath('//nav/div/nav/ul/li[2]/a')),
    EducationToolsHeader: element(by.xpath('//nav/div/nav/ul/li[3]/a')),
    SignIn: element(by.xpath('//nav/div/nav/ul/li[4]/a')),

    QualityPaymentProgramFooterTitle: element(by.xpath('//footer/div/div[1]/div/h3')),

    LearnMoreQPPTitle: element(by.xpath('//footer/div/div[2]/div[1]/h4')),

    QualityPaymentProgram: element(by.xpath('//footer/div/div[2]/div[1]/ul/li[1]/a')),
    LearnAbouttheProgramFooter: element(by.xpath('//footer/div/div[2]/div[1]/ul/li[2]/a')),
    ExploreMeasuresFooter: element(by.xpath('//footer/div/div[2]/div[1]/ul/li[3]/a')),
    EducationToolsFooter: element(by.xpath('//footer/div/div[2]/div[1]/ul/li[4]/a')),

    PolicyCenterTitle: element(by.xpath('//footer/div/div[2]/div[2]/h4')),
    CMSPrivacyNotice: element(by.xpath('//footer/div/div[2]/div[2]/ul/li[1]/a')),
    Accessibility: element(by.xpath('//footer/div/div[2]/div[2]/ul/li[2]/a')),

    ServiceCenterTitle: element(by.xpath('//footer/div/div[2]/div[3]/h4')),
    EmailContact: element(by.xpath('//footer/div/div[2]/div[3]/ul/li[1]/a')),
    EmailSubscribe: element(by.xpath('/footer/div/div[2]/div[3]/ul/li[2]/a')),

    CMSImg: element(by.xpath('//footer/div/div[2]/div[4]/img')),
    VersionNumber: element(by.id('build-version'))
};

