module.exports= {

        pageHeader: {
            ProgramPerformance: element(by.xpath('//nav/div/ul/li[1]/a')),
            QualityMeasures: element(by.xpath('//nav/div/ul/li[2]/a')),
            AdvancingCareInformation: element(by.xpath('//nav/div/ul/li[3]/a')),
            ImprovementActivities: element(by.xpath('//nav/div/ul/li[4]/a')),
        },

        programPerformancePage:{

        },
        qualityMeasuresPage:{
            searchBox: element(by.id('criteria')),
            selectMeasuresText: element(by.xpath('//section/div/div[2]/div/h2')),
            filtereAndAlldDbox: element.all(by.id('dropdown-filter-all')),
            highPriorityMeasures: element(by.xpath('//form/div[1]/div[2]/div/div/div[1]/button')),
            highPriorityMeasuresDropDown: element.all(by.xpath('//div[2]/div/div/div[1]/ul/li/a/input')),
            dataSubmissionMethod: element(by.id('dropdown-submission')),
            dataSubmissionDropDown: element.all(by.xpath('//div[1]/div[2]/div/div/div[2]/ul/li/a/input')),
            specialtyMeasureSet: element(by.id('dropdown-specialty')),
            specialtyMeasureSetDropDown: element.all(by.xpath('//div[1]/div[2]/div/div/div[3]/ul')),
            resultNumber: element(by.xpath('//section/div/div[4]/div/p/strong')),
            measuresCount: element(by.xpath('//section/div/div[5]/div[1]/div/div[2]/div[1]/strong')),

        },

        advancingCareInformationPage:{
                aciTabOne: element(by.id('aci-tab-one')),
                patientSpecificEducation: element(by.xpath('//div[2]/div[2]/div[7]/div/div[1]/p/a')),
                patientSpecificEducationText: element(by.xpath('//div[1]/div[2]/div[2]/div[7]/div/div[2]/div/p'))

        },
        improvementActivitiesPage:{

        }

        };


