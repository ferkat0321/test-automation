module.exports= {
        HowDoIParticipateInProgram: element(by.xpath('//nav/div/ul/li[1]/a')),
        HowDoIParticipateInAlternativePaymentModules: element(by.xpath('//nav/div/ul/li[2]/a')),
        WhatCanIDoNow: element(by.xpath('//nav/div/ul/li[3]/a')),
        NextParticipateInProgramButton: element(by.css('.btn.btn-continue'))
};
