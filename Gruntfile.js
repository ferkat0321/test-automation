/**
 * Created by fuerhaiti on 11/23/16.
 */

<<<<<<< HEAD
module.exports = function (grunt) {
    grunt.initConfig({
        //Let grunt read basic settings and plugins about the project
        pkg: grunt.file.readJSON('package.json'),

        clean: ['./Functional_Regression'],

        exec: {

            generateReport: {
                //This step generates allure reports in ./Functional_Regression/allure-report
                command: "allure generate ./Functional_Regression/allure_results -o ./Functional_Regression/allure-report",
            },

            // copyAllureResults: {
            //     //This step copies allure reports into a folder with today's date
            //     command: "cp -a ./Functional_Regression/allure_results ./report_history/$(date +%d-%b-%Y_%H-%M-%S)",
            // },
            // copyAllureReports: {
            //     //This step copies allure reports into a folder with today's date
            //     command: "cp -a ./Functional_Regression/allure-report ./report_history/$(date +%d-%b-%Y_%H-%M-%S)",
            // },
            openReport: {
                //This step opens report on browser.
                command: "allure report open -o ./Functional_Regression/allure-report",
            },
        },

        protractor: {

            local: {
                options: {
                    configFile: "local.conf.js",
                    // Stops Grunt process if a test fails
                    keepAlive: false,
                    noColor: false


                }
            },
            saucelabs: {
                options: {
                    configFile: "sauceLabs.conf.js",
                    noColor: false,
                    keepAlive: true
                }
            }
        }

    });

    //Load grunt plugins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-protractor-runner');

    grunt.registerTask('default', 'runs my tasks', function () {
        var tasks = ['clean', 'protractor:local', 'exec'];

        // Use the force option for all tasks declared in the previous line
        grunt.option('force', true);
        grunt.task.run(tasks);
    });
};



=======
module.exports = function(grunt) {
    grunt.initConfig({
        //Let grunt read basic seetings and plugins about the project
        pkg: grunt.file.readJSON('package.json'),

        //Create a mocha task to run test using grunt
        mochaTest: {
            test: {
                options: {
                    reporter: '',
                    captureFile: 'results.txt',
                    quiet: false,
                    clearRequireCache: false,
                    logErrors: true
                },
                src: ['./test_suite/api/sampleApitest.js']
            }
        },
        //Uglify
        uglify:{
            dist:{
                files:{
                    './test_suite/e2e/specs/sprint2/variables.min.js':['./test_suite/e2e/specs/sprint_test/variables.js']
                }
            }
        },
        mochaProtractor: {
            options: {
                browsers: ['Chrome', 'Firefox']
            },
            files: ['./test_suite/e2e/specs/regression_test/verify_learn_about_program_page_test.js']
        },
        clean:['./allure-results','./allure-report']
    });

    //Load grunt plugins
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-mocha-protractor');


    //Run the task
    grunt.registerTask('default', ['mochaTest','clean','mochaProtractor']);

};
>>>>>>> 63db5967108cab2f661bd69adeaf82935ab73588
